#@TYPE: Machine(s)
#@NAME: common include file for all (compatible) machines

#@DESCRIPTION: Machine configuration for u-boot-wic systems

##### --> please note #####
# that " u-boot-uenv-helper" needs to be added
# for .wic images in image recipe or local.conf e.g.
# IMAGE_INSTALL:append = " u-boot-uenv-helper"
##### <-- please note #####

# Recipes to build that do not provide packages for
# installing into the root filesystem but building
# the image depends on the recipes:
EXTRA_IMAGEDEPENDS += "virtual/bootloader wic-tools"

# the default MACHINE_EXTRA_RRECOMMENDS
# would not install the kernel modules on the core-image-minimal rootfs
# we could also build a core-image-basic to get them included or:
MACHINE_ESSENTIAL_EXTRA_RDEPENDS += " kernel-modules kernel-devicetree"

# --> add firmware
# if we build a core-image-minimal.wic with all the firmware,
# it grows from 170M --> 1.1G
# so we include only conditionally what's needed
MACHINE_FIRMWARE:append = " ${@bb.utils.contains("MACHINE_FEATURES", "wifi", " linux-firmware-rtl8192cu", "" ,d)}"
# with a statically linked kernel module it can't find the firmware, since the rootfs is not there yet?
MACHINE_FIRMWARE:append = " linux-firmware-imx-sdma-imx6q"
# if we want all the firmware:
#MACHINE_FIRMWARE:append = " linux-firmware"

# this adds the above to the image:
MACHINE_ESSENTIAL_EXTRA_RDEPENDS += "${MACHINE_FIRMWARE}"
# <-- add firmware

# --> compiler tuning stuff
# try this for generic armv7a image
# this is armv7 soft float - slow
# but better backwards compatible to various chips
# DEFAULTTUNE = "armv7a-neon"
# this is arm armv7 hard float - faster
# https://openbenchmarking.org/result/2009043-HU-2009045HU05
# https://openbenchmarking.org/result/2009034-HU-2009038HU98
# I guess we will use form now on
# It should work on my armv7 boards - let's see
#DEFAULTTUNE = "armv7athf"
DEFAULTTUNE = "armv7athf-neon"
require conf/machine/include/arm/arch-armv7a.inc
# <-- compiler tuning stuff

# --> rootfs stuff
IMAGE_FSTYPES += " tar.xz wic.xz wic.bmap"
# <-- rootfs stuff

# --> serial consoles
# zedboard:        ttyPS0
# sitara:          ttyO0 (ti serial driver)
# i.mx6:           ttymxc1
# i.mx6 wand:      ttymxc0
# cyclone-v:       ttyS0 (sitara)
# omap3-beagle-xm: ttyS2
# stm32mp1:        ttySTM0
# regor:           ttyS0 (sitars) - but uart1 -> RS485, so don't use it here

# see https://bugzilla.yoctoproject.org/show_bug.cgi?id=15337
# this is broken:
# SERIAL_CONSOLES ?= "115200;ttyPS0 115200;ttyO0 115200;ttymxc1 115200;ttymxc0 115200;ttyS0 115200;ttyS2 115200;ttySTM0"
# SERIAL_CONSOLES_CHECK ?= "${SERIAL_CONSOLES}"
# let's try without SERIAL_CONSOLES_CHECK, but with
SERIAL_CONSOLES ?= "115200;console"
# The theory:
# Note that if you boot without a console= option (or with console=/dev/tty0), /dev/console is the same as /dev/tty0.
# ... but we always pass a console option via u-boot, so this should work
# <-- serial consoles

# --> kernel stuff

# kernel version

#KERNEL_VERSION_PATCHLEVEL ?= "5.10"
#KERNEL_SUBLEVEL ?= "27"
# let's try phytec special version:
#KERNEL_VERSION_PATCHLEVEL ?= "5.15"
#KERNEL_SUBLEVEL ?= "35"
# let's try for fun 5.15.x:
# --> Phytec special + upstream
#KERNEL_VERSION_PATCHLEVEL ?= "5.15"
#KERNEL_SUBLEVEL ?= "63"
#KERNEL_SUBLEVEL ?= "81"
# <-- Phytec special + upstream
#KERNEL_VERSION_PATCHLEVEL ?= "5.15"
#KERNEL_SUBLEVEL ?= "69"
# let's try for fun 6.1.1
# gcc 13 needs a more recent 6.1.x:
#KERNEL_VERSION_PATCHLEVEL ?= "6.1"
#KERNEL_SUBLEVEL ?= "1"
#KERNEL_SUBLEVEL ?= "22"
#KERNEL_SUBLEVEL ?= "37"
#KERNEL_SUBLEVEL ?= "42"
#KERNEL_SUBLEVEL ?= "71"
# let's try for fun 6.6.15
#KERNEL_VERSION_PATCHLEVEL ?= "6.6"
#KERNEL_SUBLEVEL ?= "15"
#KERNEL_SUBLEVEL ?= "22"
# let's try for fun 6.12.0-rc6
# KERNEL_VERSION_PATCHLEVEL ?= "6.12"
# KERNEL_SUBLEVEL ?= "0-rc6"
#KERNEL_VERSION_PATCHLEVEL ?= "6.12"
#KERNEL_SUBLEVEL ?= "0"
KERNEL_VERSION_PATCHLEVEL ?= "6.12"
KERNEL_SUBLEVEL ?= "5"

PREFERRED_VERSION_linux-yocto-custom = "${KERNEL_VERSION_PATCHLEVEL}.${KERNEL_SUBLEVEL}%"

# don't comment the following out to
# use instead of "default" version of linux libc headers
# this version which is an exact fit to the kernel version
# here we don't try to use custom linux-libc-headers
# LINUXLIBCVERSION ?= "${KERNEL_VERSION_PATCHLEVEL}"

# --> standard/debug/virt
ML_DEFAULT_KERNEL := "linux-yocto-custom"
# we use a mainline defconfig and apply config fragments
# choose KTYPE for different sets of config fragments
# default KTYPE:
KTYPE ?= "std"
#KTYPE ?= "debug"
#KTYPE ?= "up"
# if we use the resy-virt distro it includes conf/distro/include/ktype-virt.inc
# which sets KTYPE="virt"

# If you do not specify a LINUX_KERNEL_TYPE,
# it defaults to "standard"
LINUX_KERNEL_TYPE ?= "${KTYPE}"
# <-- standard/debug/virt

# --> prt
#PRT_KERNEL_VERSION_PATCHLEVEL="3.18"
#PRT_KERNEL_SUBLEVEL="20"
#ML_DEFAULT_KERNEL := "linux-yocto-custom"
#PREFERRED_VERSION_linux-yocto-custom = "${PRT_KERNEL_VERSION_PATCHLEVEL}.${PRT_KERNEL_SUBLEVEL}%"
#KTYPE ?= "preempt-rt"
# <-- prt

# default kernel provider
PREFERRED_PROVIDER_virtual/kernel ??= "${ML_DEFAULT_KERNEL}"

KERNEL_PACKAGE_NAME:pn-linux-yocto-custom-up = "up-linux"
KERNEL_PACKAGE_NAME:pn-linux-yocto-custom-std = "std-linux"
KERNEL_PACKAGE_NAME:pn-linux-yocto-custom-debug = "debug-linux"
KERNEL_PACKAGE_NAME:pn-linux-yocto-custom-virt = "virt-linux"
KERNEL_PACKAGE_NAME:pn-linux-yocto-custom-xenomai = "xenomai-linux"
KERNEL_PACKAGE_NAME:pn-linux-yocto-custom-prt = "prt-linux"

KERNEL_IMAGETYPE ?= "uImage"
#KERNEL_IMAGETYPE_stm32mp157c-dk2 ?= "zImage"
# vmlinux needed for crosstap
KERNEL_ALT_IMAGETYPE ?= "vmlinux"

# in case we don't want LOAD_ADDR hardcoded:
# - need CONFIG_AUTO_ZRELADDR=y
# - need to compile kernel with UIMAGE_TYPE=kernel_noload
# - need in u-boot to load it at 0x80000000 + 0x8000 - 0x40 (U-boot header)
# - here it happens to work also with KERNEL_ADDR_R="0x80300000"

# replaced :
# -->
# hardcoded LOADADDR
#UBOOT_ENTRYPOINT = "0x80008000"
#UBOOT_LOADADDRESS = "0x80008000"
#KERNEL_EXTRA_ARGS += "LOADADDR=${UBOOT_ENTRYPOINT}"
# <--

# with:
# -->
# not hardcoded LOADADDR
UBOOT_ENTRYPOINT ?= "0x0"
# not sure this is needed, since we build zImage
#UBOOT_ENTRYPOINT_stm32mp157c-dk2 ?= "0xC2000040"
UBOOT_LOADADDRESS ?= "0x0"
KERNEL_EXTRA_ARGS += "LOADADDR=${UBOOT_ENTRYPOINT} UIMAGE_TYPE=kernel_noload"
# <--

# --> kernel stuff
# in order to pick up the multi-v7-ml config stuff
KMACHINE ?= "multi-v7-ml"
#KMACHINE_am335x-phytec-wega = "multi-v7-ml"
#KMACHINE_imx6q-phytec-mira-rdk-nand = "multi-v7-ml"
#KMACHINE_beagle-bone-black = "multi-v7-ml"
KMACHINE_beagle-bone-green = "multi-v7-ml"
#KMACHINE_imx6ul-phytec-segin = "multi-v7-ml"
KMACHINE_am335x-pocketbeagle = "multi-v7-ml"
#KMACHINE_omap3-beagle-xm = "multi-v7-ml"
#KMACHINE_imx6sx-udoo-neo-full = "multi-v7-ml"
#KMACHINE_stm32mp157c-dk2 = "multi-v7-ml"
#KMACHINE_de0-nano-soc-kit = "multi-v7-ml"

#KMACHINE_am335x-regor-rdk = "multi-v7-ml"
# <-- kernel stuff

# --> device tree stuff
#KERNEL_DEVICETREE_am335x-phytec-wega = "am335x-wega-rdk.dtb"
#KERNEL_DEVICETREE_imx6q-phytec-mira-rdk-nand = "imx6q-phytec-mira-rdk-nand.dtb"
#KERNEL_DEVICETREE_beagle-bone-black = "am335x-boneblack.dtb"

#KERNEL_DEVICETREE_beagle-bone-green = "am335x-bonegreen.dtb"
KERNEL_DEVICETREE_beagle-bone-green ?= "${@ 'ti/omap/am335x-bonegreen.dtb' if [int(i) for i in (d.getVar('KERNEL_VERSION_PATCHLEVEL') or '0.0').split('.')] > [6, 5] else 'am335x-bonegreen.dtb' }"

#KERNEL_DEVICETREE_imx6ul-phytec-segin = "imx6ul-phytec-segin-ff-rdk-nand.dtb"

#KERNEL_DEVICETREE_am335x-pocketbeagle = "am335x-pocketbeagle.dtb"
KERNEL_DEVICETREE_am335x-pocketbeagle ?= "${@ 'ti/omap/am335x-pocketbeagle.dtb' if [int(i) for i in (d.getVar('KERNEL_VERSION_PATCHLEVEL') or '0.0').split('.')] > [6, 5] else 'am335x-pocketbeagle.dtb' }"

#KERNEL_DEVICETREE_omap3-beagle-xm = "omap3-beagle-xm.dtb"
#KERNEL_DEVICETREE_imx6sx-udoo-neo-full = "imx6sx-udoo-neo-full.dtb"
#KERNEL_DEVICETREE_stm32mp157c-dk2 = "stm32mp157c-dk2.dtb"
#KERNEL_DEVICETREE_de0-nano-soc-kit = "socfpga_cyclone5_de0_nano_soc.dtb"

#KERNEL_DEVICETREE_am335x-regor-rdk = "am335x-regor-rdk.dtb"
# <-- device tree stuff

# --> u-boot-stuff
# items affected
#    u-boot
#    virtual/bootloader
#    u-boot-dev
#    u-boot-env
#    u-boot-extlinux
#    u-boot-tools-native
# 
# u-boot is used here!
#DAS_UBOOT_VERSION ?= "2020.10%"
#DAS_UBOOT_VERSION ?= "2021.01%"
#DAS_UBOOT_VERSION ?= "2021.04%"
#DAS_UBOOT_VERSION ?= "2021.07%"
#DAS_UBOOT_VERSION ?= "2021.10%"
#DAS_UBOOT_VERSION ?= "2022.01%"
#DAS_UBOOT_VERSION ?= "2022.04%"
#DAS_UBOOT_VERSION ?= "2022.07%"
#DAS_UBOOT_VERSION ?= "2022.10%"
#DAS_UBOOT_VERSION ?= "2023.01%"
#DAS_UBOOT_VERSION ?= "2023.04%"
#DAS_UBOOT_VERSION ?= "2023.07.02%"
#DAS_UBOOT_VERSION ?= "2023.10%"
#DAS_UBOOT_VERSION ?= "2024.01%"
#DAS_UBOOT_VERSION ?= "2024.04%"
#DAS_UBOOT_VERSION ?= "2024.07%"
DAS_UBOOT_VERSION ?= "2024.10%"
PREFERRED_VERSION_u-boot ?= "${DAS_UBOOT_VERSION}"
PREFERRED_PROVIDER_u-boot-mkimage-native ?= "u-boot-tools-native"
# so we don't accidentally pick up the one from poky
PREFERRED_VERSION_u-boot-tools-native ?= "${DAS_UBOOT_VERSION}"

# UBOOT_SUFFIX:
UBOOT_SUFFIX ?= "img"
#UBOOT_SUFFIX_de0-nano-soc-kit = "sfp"

# SPL_BINARY name:
#SPL_BINARYNAME_am335x-phytec-wega = "MLO"
#SPL_BINARYNAME_imx6q-phytec-mira-rdk-nand = "SPL"
#SPL_BINARYNAME_beagle-bone-black = "MLO"
SPL_BINARYNAME_beagle-bone-green = "MLO"
#SPL_BINARYNAME_imx6ul-phytec-segin = "SPL"
SPL_BINARYNAME_am335x-pocketbeagle = "MLO"
#SPL_BINARYNAME_omap3-beagle-xm = "MLO"
#SPL_BINARYNAME_imx6sx-udoo-neo-full = "SPL"
#SPL_BINARYNAME_stm32mp157c-dk2 = "u-boot-spl.stm32"
#SPL_BINARYNAME_de0-nano-soc-kit = "u-boot-with-spl.sfp"

#SPL_BINARYNAME_am335x-regor-rdk = "MLO"
# not sure about that SPL for de0-nano

SPL_BINARY = "${SPL_BINARYNAME}"

# UBOOT defconfig:
# I think this:
# UBOOT_MACHINE_am335x-phytec-wega = "pcm051_rev3_defconfig"
# was with 2020.10? replaced by:
#UBOOT_MACHINE_am335x-phytec-wega = "phycore-am335x-r2-wega_defconfig"
#UBOOT_MACHINE_imx6q-phytec-mira-rdk-nand = "pcm058_defconfig"
#UBOOT_MACHINE_beagle-bone-black = "am335x_evm_defconfig"
UBOOT_MACHINE_beagle-bone-green = "am335x_evm_defconfig"
#UBOOT_MACHINE_imx6ul-phytec-segin = "phycore_pcl063_defconfig"
UBOOT_MACHINE_am335x-pocketbeagle = "am335x_evm_defconfig"
#UBOOT_MACHINE_omap3-beagle-xm = "omap3_beagle_defconfig"
#UBOOT_MACHINE_imx6sx-udoo-neo-full = "udoo_neo_defconfig"
#UBOOT_MACHINE_stm32mp157c-dk2 = "stm32mp15_basic_defconfig"
#UBOOT_MACHINE_de0-nano-soc-kit = "socfpga_de0_nano_soc_defconfig"
#UBOOT_MACHINE_am335x-regor-rdk = "phycore-am335x-r2-regor_defconfig"

#UBOOT_MAKE_TARGET_stm32mp157c-dk2 = "DEVICE_TREE=stm32mp157c-dk2 all"
#UBOOT_MACHINE_stm32mp157c-dk2 = "stm32mp15_basic_defconfig"
#UBOOT_EXTLINUX_stm32mp157c-dk2 = "1"
#UBOOT_EXTLINUX_FDT_stm32mp157c-dk2 = "/boot/stm32mp157c-dk2.dtb"
#UBOOT_EXTLINUX_ROOT_stm32mp157c-dk2 = "root=/dev/mmcblk0p4"
#UBOOT_EXTLINUX_CONSOLE_stm32mp157c-dk2 = ""

# <-- u-boot-stuff

# --> wks files stuff
#WKS_FILE_am335x-phytec-wega ?= "am335x-phytec-wega-sd-card.wks.in"
#WKS_FILE_imx6q-phytec-mira-rdk-nand ?= "imx6q-phytec-mira-rdk-nand-sd-card.wks.in"
#WKS_FILE_beagle-bone-black ?= "beagle-bone-black.wks.in"
WKS_FILE_beagle-bone-green ?= "beagle-bone-green.wks.in"
#WKS_FILE_imx6ul-phytec-segin ?= "imx6ul-phytec-segin-sd-card.wks.in"
WKS_FILE_am335x-pocketbeagle ?= "am335x-pocketbeagle-sd-card.wks"
#WKS_FILE_omap3-beagle-xm ?= "omap3-beagle-xm-sd-card.wks.in"
#WKS_FILE_imx6sx-udoo-neo-full ?= "imx6sx-udoo-neo-full-sd-card.wks.in"
#WKS_FILE_stm32mp157c-dk2 ?= "stm32mp157c-dk2-sd-card.wks.in"
#WKS_FILE_de0-nano-soc-kit ?= "de0-nano-soc-kit-sd-card.wks.in"

#WKS_FILE_am335x-regor-rdk ?= "am335x-regor-rdk-sd-card.wks.in"
do_image_wic[depends] += "mtools-native:do_populate_sysroot dosfstools-native:do_populate_sysroot"

# Do not update fstab file when using wic images
#WIC_CREATE_EXTRA_ARGS_stm32mp157c-dk2 ?= "--no-fstab-update"
#
#WKS_FILE_DEPENDS_stm32mp157c-dk2 ?= " \
#    virtual/bootloader \
#    e2fsprogs-native \
#    bmap-tools-native \
#"

## Define specific EXT4 command line:
##   - Create minimal inode number (as it is done by default in image_types.bbclass)
##   - Deactivate metadata_csum not supported by U-Boot
## EXTRA_IMAGECMD:ext4 = "-i 4096 -O ^metadata_csum"

# --> 
# by default:
UBOOT_BINARY ?= "u-boot.${UBOOT_SUFFIX}"
#UBOOT_BINARY_de0-nano-soc-kit = "u-boot-with-spl.${UBOOT_SUFFIX}"
# looks like it was renamed?
# at the moment handled in u-boot-uenv-helper
# UBOOT_BINARY is copied as u-boot.bin and u-boot-dtb.bin
# UBOOT_BINARY_imx6q-phytec-mira-rdk-nand = "u-boot-dtb.${UBOOT_SUFFIX}"
# <-- 

IMAGE_BOOT_FILES ?= "${UBOOT_BINARY} ${SPL_BINARYNAME}"
# <-- wks files stuff

# --> MACHINE_FEATURES
##########################################################################
# you need to include packagegroup-base and/or packagegroup-base-extended
# MACHINE/DISTRO_FEATURES to install stuff on the rootfs
##########################################################################
# default MACHINE_FEATURES
MACHINE_FEATURES = "usbgadget usbhost vfat"

#MACHINE_FEATURES_stm32mp157c-dk2 = "usbhost alsa ext2 bluetooh" 

# Let's add wifi support - make sure that also DISTRO_FEATURES include "wifi"
MACHINE_FEATURES += "wifi"

#MACHINE_ESSENTIAL_EXTRA_RDEPENDS_stm32mp157c-dk2 += " \
#    kernel-image \
#    kernel-devicetree \
#    kernel-modules \
#    u-boot \
#    linux-firmware-bcm43430 \
#"
# <-- MACHINE_FEATURES

# --> container stuff - default values 
# if we build without docker/podman
# just create the brtfs partition from .wic.in file
# and do not mount it
CONTAINER_ON_PERSISTENT ?= "0"
CONTAINER_ON_PERSISTENT_FSTYPE ?= "btrfs"
DEVICE_FOR_CONTAINER ?= "/dev/mmcblk0p3"
MOUNT_FOR_CONTAINER ?= "/var/lib/docker"
CONTAINER_WIC_ONDISK ?= "mmcblk0"
# <-- container stuff - default values
