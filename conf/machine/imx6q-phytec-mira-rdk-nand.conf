#@TYPE: Machine
#@NAME: imx6q-phytec-mira-rdk-nand

#@DESCRIPTION: Machine configuration for imx6q-phytec-mira-rdk-nand systems

# --> serial consoles
# zedboard:        ttyPS0
# sitara:          ttyO0 (ti serial driver)
# i.mx6:           ttymxc1
# i.mx6 wand:      ttymxc0
# cyclone-v:       ttyS0 (sitara)
# omap3-beagle-xm: ttyS2
# stm32mp1:        ttySTM0
# regor:           ttyS0 (sitara) - but uart1 -> RS485, so don't use it here

# override what's in common.inc:
SERIAL_CONSOLES = "115200;ttymxc1"
SERIAL_CONSOLES_CHECK = "${SERIAL_CONSOLES}"
# <-- serial consoles

# move from common.inc here:

# --> kernel stuff
KMACHINE = "multi-v7-ml"
# <-- kernel stuff

# --> device tree stuff
# old kernel - device tree structure:
#KERNEL_DEVICETREE ?= "imx6q-phytec-mira-rdk-nand.dtb"
#KERNEL_DEVICETREE ?= "imx6q-phytec-mira-ff-rdk-nand.dtb"
# new kernel - device tree structure:
# No rule to make target 'arch/arm/boot/dts/imx6q-phytec-mira-rdk-nand.dtb'.  Stop.
#KERNEL_DEVICETREE ?= "nxp/imx/imx6q-phytec-mira-rdk-nand.dtb"

# if the kernel version is bigger than 6.5 -> nxp/imx/imx6q-phytec-mira-rdk-nand.dtb
# else imx6q-phytec-mira-rdk-nand.dtb
KERNEL_DEVICETREE ?= "${@ 'nxp/imx/imx6q-phytec-mira-rdk-nand.dtb' if [int(i) for i in (d.getVar('KERNEL_VERSION_PATCHLEVEL') or '0.0').split('.')] > [6, 5] else 'imx6q-phytec-mira-rdk-nand.dtb' }"
# <-- device tree stuff

# --> u-boot-stuff
SPL_BINARYNAME = "SPL"
UBOOT_MACHINE = "pcm058_defconfig"
# <-- u-boot-stuff

# --> wks files stuff
WKS_FILE ?= "imx6q-phytec-mira-rdk-nand-sd-card.wks.in"
# <-- wks files stuff

# --> custom u-boot version because later version does not work?
#DAS_UBOOT_VERSION ?= "2023.01%"
# <-- custom u-boot version because later version does not work?

# include the common stuff
include common.inc
