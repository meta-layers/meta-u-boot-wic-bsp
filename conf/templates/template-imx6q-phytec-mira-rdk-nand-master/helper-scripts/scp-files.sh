#!/bin/bash

DEPLOY="imx6q-phytec-mira-rdk-nand-wic-master/tmp/deploy/images/imx6q-phytec-mira-rdk-nand"

pushd /workdir/build/${DEPLOY}

#SOURCE_1="core-image-minimal-imx6q-phytec-mira-rdk-nand-*.rootfs.wic*"
#SOURCE_1="core-image-minimal-imx6q-phytec-mira-rdk-nand.rootfs-*wic*"
SOURCE_1="core-image-minimal-imx6q-phytec-mira-rdk-nand*rootfs*??????????????*wic*"
SOURCE_2="core-image-minimal-base-imx6q-phytec-mira-rdk-nand*rootfs*??????????????*wic*"
SOURCE_3="core-image-minimal-base-tig-prebuilt-imx6q-phytec-mira-rdk-nand*rootfs*??????????????*wic*"
SOURCE_4="core-image-minimal-telegraf-prebuilt-imx6q-phytec-mira-rdk-nand*rootfs*??????????????*wic*"
SOURCE_5="core-image-minimal-base-tig-plus-plus-prebuilt*rootfs*??????????????*wic*"

TARGET_1="student@192.168.42.60:/home/student/projects/${DEPLOY}"

set -x
scp -r ${SOURCE_1} ${TARGET_1}
scp -r ${SOURCE_2} ${TARGET_1}
scp -r ${SOURCE_3} ${TARGET_1}
scp -r ${SOURCE_4} ${TARGET_1}
scp -r ${SOURCE_5} ${TARGET_1}
set +x

popd
