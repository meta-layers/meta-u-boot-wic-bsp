# Objectives

Install and test the extensible SDK.

# Minimal build framework

@@@ TODO: Setup a minimal build framework.

# Install the eSDK

## Create top level install dir

on buildhost (once):

Create a dir where we will install the SDK to:

```
[@buildhost:]
pushd /opt/resy
sudo mkdir imx6q-phytec-mira-rdk-nand
sudo chown ${USER}:${USER} imx6q-phytec-mira-rdk-nand
popd
```


## Get into the SDK container

```
[@buildhost:]
cd /workdir
./resy-sdk-container.sh
```

## Install buildtools extended (once)

In the container:

```
[@sdk-container:/ ]$
cd /workdir/build/imx6q-phytec-mira-rdk-nand-wic-master/tmp/deploy/sdk/
time ./x86_64-buildtools-extended-nativesdk-standalone-3.3+snapshot-5dce2f3da20a14c0eb5229696561b0c5f6fce54c.sh
install into: 
/opt/resy/imx6q-phytec-mira-rdk-nand/buildtools-extended
```

you should see something like this:
```
[@sdk-container:/ ]$
Extended Build tools installer version 3.3+snapshot
===================================================
Enter target directory for SDK (default: /opt/resy/3.3+snapshot): /opt/resy/imx6q-phytec-mira-rdk-nand/buildtools-extended
You are about to install the SDK to "/opt/resy/imx6q-phytec-mira-rdk-nand/buildtools-extended". Proceed [Y/n]? Y
Extracting SDK...................................done
Setting it up...done
SDK has been successfully set up and is ready to be used.
Each time you wish to use the SDK in a new shell session, you need to source the environment setup script e.g.
 $ . /opt/resy/imx6q-phytec-mira-rdk-nand/buildtools-extended/environment-setup-x86_64-resysdk-linux

real    0m38.719s
user    0m16.796s
sys     0m4.372s
```

## Setup buildtools extended environment (each time)

setup the environment for buildtools-extended:
```
[@sdk-container:/ ]$
source /opt/resy/imx6q-phytec-mira-rdk-nand/buildtools-extended/environment-setup-x86_64-resysdk-linux
```

you should see something like this:
```
[@sdk-container:/ ]$
Icecc not found. Disabling distributed compiling
```

## Install eSDK (once)

```
[@sdk-container:/ ]$
cd /workdir/build/imx6q-phytec-mira-rdk-nand-wic-master/tmp/deploy/sdk/
time ./resy-glibc-x86_64-core-image-minimal-armv7at2hf-neon-imx6q-phytec-mira-rdk-nand-toolchain-ext-3.3+snapshot.sh
install into:
/opt/resy/imx6q-phytec-mira-rdk-nand/esdk
```

you should see/enter something like this:

```
[@sdk-container:/ ]$
Resy (Reliable Embedded Systems Reference Distro) Extensible SDK installer version 3.3+snapshot
===============================================================================================
Enter target directory for SDK (default: ~/resy_sdk): /opt/resy/imx6q-phytec-mira-rdk-nand/esdk
You are about to install the SDK to "/opt/resy/imx6q-phytec-mira-rdk-nand/esdk". Proceed [Y/n]? Y
Extracting SDK...................................................................done
Setting it up...
Extracting buildtools...
Preparing build system...
Loading cache: 100% |                                                                                                                                                         | ETA:  --:--:--
Loaded 0 entries from dependency cache.
WARNING: /opt/resy/imx6q-phytec-mira-rdk-nand/esdk/layers/meta-u-boot-wic-bsp-master/recipes-bsp/u-boot-env-helper/u-boot-env-helper_1.0.bb: Unable to get checksum for u-boot-env-helper SRC_URI entry uboot.env: file could not be found
NOTE: imported (AUTOINC-linux-yocto-custom-5.10.27-std+git,imx6q_phytec_mira_rdk_nand,AUTOINC+472493c8a4,0)################################################################### | ETA:  0:00:00
NOTE: imported (linux-yocto-custom-5.10.27-std+gitAUTOINC+472493c8a4-r0,imx6q_phytec_mira_rdk_nand,5d239fdd515ca604a3a808f26ba021243587287806f111e63aafd1caeaaa88ad,0)
Parsing recipes: 100% |########################################################################################################################################################| Time: 0:03:55
Parsing of 2432 .bb files complete (0 cached, 2432 parsed). 3751 targets, 143 skipped, 0 masked, 0 errors.

Summary: There was 1 WARNING message shown.
Importing from file /opt/resy/imx6q-phytec-mira-rdk-nand/esdk/conf/prserv.inc succeeded!
Loading cache: 100% |                                                                                                                                                         | ETA:  --:--:--
WARNING: /opt/resy/imx6q-phytec-mira-rdk-nand/esdk/layers/meta-u-boot-wic-bsp-master/recipes-bsp/u-boot-env-helper/u-boot-env-helper_1.0.bb: Unable to get checksum for u-boot-env-helper SRC_URI entry uboot.env: file could not be found
Parsing recipes: 100% |########################################################################################################################################################| Time: 0:02:46
Initialising tasks: 100% |#####################################################################################################################################################| Time: 0:00:03
Checking sstate mirror object availability: 100% |#############################################################################################################################| Time: 0:00:00
WARNING: The at-spi2-core:do_write_config sig is computed to be 415b4f95bc467c598e93db8d23a935b393f465bbadf11b689391cf9db0101c3b, but the sig is locked to 3b3358dea476e8a408e56ac19e6af65772077a72e8f4716ee53a42bb8d84f43d in SIGGEN_LOCKEDSIGS_t-armv7at2hf-neon
The libgcc:do_fetch sig is computed to be 6b78d2eb3867ea973e9c7eee92034a89526765b5be364438f13c7a4773a227a0, but the sig is locked to ac749dad0603c033af49a4034733385550f5f46ceba3475b31221d1f93a67487 in SIGGEN_LOCKEDSIGS_t-armv7at2hf-neon
The pango:do_write_config sig is computed to be 17f1a1b6c36c39112ecb43accc93ff1c845af541f39b344882725d90c6d33f91, but the sig is locked to 85b9b2cd1acb8ad0d99196298ce52127f83a7ba9b0f991f716f44ea1fd7e1372 in SIGGEN_LOCKEDSIGS_t-armv7at2hf-neon
...
The adwaita-icon-theme:do_populate_sysroot sig is computed to be 98bd40f5e1b76259cee36dac6799768a761ca3b1e66f00e378259a4767b5514c, but the sig is locked to 2796debd0a9427f96cd00b4656045f7b2d21fad9b20c970dbffd889b0d15340d in SIGGEN_LOCKEDSIGS_t-allarch
The vim:do_packagedata sig is computed to be df631aa168525993a4f1b9304865f73f65b98b3e8edd2202acda07d1265d3fe2, but the sig is locked to 652e543de23169402d268f047c6b5aeaaab78ca57a445875dad5f30381fdb366 in SIGGEN_LOCKEDSIGS_t-armv7at2hf-neon
The adwaita-icon-theme:do_packagedata sig is computed to be becbc688f70f16cf62c03d912a8b095fe22ec81d3f4b8342185d29dbe8d5e31f, but the sig is locked to 5b2f34916c6d739b05cda2060cc2cd6c8f4a716a93a4e67ec308d3287dad7fe6 in SIGGEN_LOCKEDSIGS_t-allarch
Loading cache: 100% |##########################################################################################################################################################| Time: 0:00:01
Parsing recipes: 100% |########################################################################################################################################################| Time: 0:00:01
Initialising tasks: 100% |#####################################################################################################################################################| Time: 0:00:00
done
SDK has been successfully set up and is ready to be used.
Each time you wish to use the SDK in a new shell session, you need to source the environment setup script e.g.
 $ . /opt/resy/imx6q-phytec-mira-rdk-nand/esdk/environment-setup-armv7at2hf-neon-resy-linux-gnueabi

real    11m28.414s
user    0m42.848s
sys     0m11.992s
```

## Setup SDK environment in SDK container (each time)

Setup the SDK environment in the container:

```
[@sdk-container:/ ]$
source /opt/resy/imx6q-phytec-mira-rdk-nand/esdk/environment-setup-armv7at2hf-neon-resy-linux-gnueabi
```

You should see:

```
[@sdk-container:/ ]$
Icecc not found. Disabling distributed compiling
SDK environment now set up; additionally you may now run devtool to perform development tasks.
Run devtool --help for further details.
```

# Try out the eSDK

## Get kernel sources
```
[@sdk-container:/ ]$
time devtool modify linux-yocto-custom
```

You should see:

```
[@sdk-container:/ ]$
NOTE: Starting bitbake server...
NOTE: Reconnecting to bitbake server...
NOTE: Retrying server connection (#1)...
Loading cache: 100% |##########################################################################################################################################################| Time: 0:00:00
Loaded 3750 entries from dependency cache.
Parsing recipes: 100% |########################################################################################################################################################| Time: 0:00:01
Parsing of 2432 .bb files complete (2431 cached, 1 parsed). 3751 targets, 143 skipped, 0 masked, 0 errors.
NOTE: Resolving any missing task queue dependencies
Initialising tasks: 100% |#####################################################################################################################################################| Time: 0:00:00
Sstate summary: Wanted 2 Local 0 Network 0 Missed 2 Current 92 (0% match, 97% complete)
WARNING: The quilt-native:do_configure sig is computed to be 7824d10c445c7c22febfbd0308a8807df9ac8911c7a9d6a012f3d0bfff754e24, but the sig is locked to 56c96f467f80f8a9c0ffab3e4def8e4409486f8664d2dd85d223cc1671d9c51a in SIGGEN_LOCKEDSIGS_t-x86-64
The quilt-native:do_compile sig is computed to be 4a845508879e1d307f87fe8425af0652c0313d3ddfd0c63681b718523d002358, but the sig is locked to 7f79166e6c0cc5863fe4835bc7c9a4ec32bd410876360c2f97f0ef9d37a2b5a5 in SIGGEN_LOCKEDSIGS_t-x86-64
...
The kmod-native:do_configure sig is computed to be 69cc2580091bbd34c30fc037dbf3dd5032251a95f45f05894d26403c7194986e, but the sig is locked to cf03d432ecbafe0c76f63e968bc7bb5dc400247a9cbb17c4a882270d2c3af783 in SIGGEN_LOCKEDSIGS_t-x86-64
The kmod-native:do_compile sig is computed to be cac905ec1b3dfdc9c86a7c1aa44041af3ea15b2ccaa24653e81669138fdc7ba5, but the sig is locked to fdd725f075ebec8768b58e7f3b6583c320f4caf5ee2d1467c19d13d94b57f7b3 in SIGGEN_LOCKEDSIGS_t-x86-64
The kmod-native:do_install sig is computed to be b7a977c45bbad70387b44a3a9f2c1f2325fe3fc8a20be9b0557f367029472f07, but the sig is locked to 4e0e92cb51615c8ca9ddc85489376895ce2b42a3b8450c17a252d07991a83eb7 in SIGGEN_LOCKEDSIGS_t-x86-64
NOTE: Executing Tasks
NOTE: Tasks Summary: Attempted 428 tasks of which 417 didn't need to be rerun and all succeeded.
INFO: Adding local source files to srctree...
INFO: Copying kernel config to srctree
INFO: Source tree extracted to /opt/resy/imx6q-phytec-mira-rdk-nand/esdk/workspace/sources/linux-yocto-custom
INFO: Recipe linux-yocto-custom now set up to build from /opt/resy/imx6q-phytec-mira-rdk-nand/esdk/workspace/sources/linux-yocto-custom

real    19m29.324s
user    0m2.328s
sys     0m1.444s
```

## A little hack concerning kernel meta-data

We make a symlink to some kernel patches, which `devtool` did not figure out.

```
[@sdk-container:/ ]$
pushd /opt/resy/imx6q-phytec-mira-rdk-nand/esdk/workspace/sources/linux-yocto-custom/oe-local-files
# this might take some time in my container the first time, since git will try to figure out the branch
rsync -avp ../../../../layers/meta-multi-v7-ml-bsp-master/recipes-kernel/linux/patch/5.10.x/patches .
popd
```

# map virtual/kernel -> linux-yocto-custom

```
[@sdk-container:/ ]$
time devtool modify virtual/kernel
```

you should see something like:

```
[@sdk-container:/ ]$
NOTE: Starting bitbake server...
NOTE: Reconnecting to bitbake server...
NOTE: Retrying server connection (#1)...
Loading cache: 100% |##########################################################################################################################################################| Time: 0:00:00
Loaded 3750 entries from dependency cache.
Parsing recipes: 100% |########################################################################################################################################################| Time: 0:00:01
Parsing of 2432 .bb files complete (2430 cached, 2 parsed). 3751 targets, 143 skipped, 0 masked, 0 errors.
Removing 1 recipes from the imx6q_phytec_mira_rdk_nand sysroot: 100% |#########################################################################################################| Time: 0:00:00
INFO: Mapping virtual/kernel to linux-yocto-custom
ERROR: recipe linux-yocto-custom is already in your workspace

real    0m8.643s
user    0m1.168s
sys     0m0.184s
```


## Build the kernel

```
[@sdk-container:/ ]$
time devtool build linux-yocto-custom
```

You should see:

```
[@sdk-container:/ ]$
NOTE: Starting bitbake server...
NOTE: Reconnecting to bitbake server...
NOTE: Retrying server connection (#1)...
Loading cache: 100% |##########################################################################################################################################################| Time: 0:00:00
Loaded 3750 entries from dependency cache.
Parsing recipes: 100% |########################################################################################################################################################| Time: 0:00:01
Parsing of 2432 .bb files complete (2430 cached, 2 parsed). 3751 targets, 143 skipped, 0 masked, 0 errors.
Loading cache: 100% |##########################################################################################################################################################| Time: 0:00:14
Loaded 3750 entries from dependency cache.
Parsing recipes: 100% |########################################################################################################################################################| Time: 0:00:00
Parsing of 2432 .bb files complete (2430 cached, 2 parsed). 3751 targets, 143 skipped, 0 masked, 0 errors.
NOTE: Resolving any missing task queue dependencies
Initialising tasks: 100% |#####################################################################################################################################################| Time: 0:00:04
Sstate summary: Wanted 1 Local 0 Network 0 Missed 1 Current 134 (0% match, 99% complete)
WARNING: The quilt-native:do_configure sig is computed to be 7824d10c445c7c22febfbd0308a8807df9ac8911c7a9d6a012f3d0bfff754e24, but the sig is locked to 56c96f467f80f8a9c0ffab3e4def8e4409486f8664d2dd85d223cc1671d9c51a in SIGGEN_LOCKEDSIGS_t-x86-64
The quilt-native:do_compile sig is computed to be 4a845508879e1d307f87fe8425af0652c0313d3ddfd0c63681b718523d002358, but the sig is locked to 7f79166e6c0cc5863fe4835bc7c9a4ec32bd410876360c2f97f0ef9d37a2b5a5 in SIGGEN_LOCKEDSIGS_t-x86-64
The quilt-native:do_install sig is computed to be e8f8623975848149f9b559e99a6c2148b02440deea8711af41c4ac287af2ef52, but the sig is locked to 6cdc05c965d2a3c6668f47c3bff999f7eb1117914accb5f5160308b442c5d091 in SIGGEN_LOCKEDSIGS_t-x86-64
...
The rpm-native:do_compile sig is computed to be 25f0a4d60dff5a9b95dd980028a37ec434c764285f9039494526717845940b74, but the sig is locked to af3c171fbe3138cc29a86d35377abc7fc1d15ec030eca66d26e9ccfc1d8d8b64 in SIGGEN_LOCKEDSIGS_t-x86-64
The rpm-native:do_install sig is computed to be 4ce64ab77ff8f2b2b706fa7c2cab1b3571d5c6a4b992440d1bf7ba6aeff36cec, but the sig is locked to 2a2e605a1b743b09bdadacccba8a1b2f36aa27799150dd47800ef87b70acb9f1 in SIGGEN_LOCKEDSIGS_t-x86-64
The dwarfsrcfiles-native:do_install sig is computed to be 9e1018cfa9c7799baf67f66919f06aab8f831f6c3a5e5b747b25eaded11a8e18, but the sig is locked to 4bd60c48fdc2252d192ba1f7f51bb7570b78be4ae2e1caadb15bbfedf5fe186d in SIGGEN_LOCKEDSIGS_t-x86-64
NOTE: Executing Tasks
NOTE: linux-yocto-custom: compiling from external source tree /opt/resy/imx6q-phytec-mira-rdk-nand/esdk/workspace/sources/linux-yocto-custom
NOTE: Tasks Summary: Attempted 628 tasks of which 606 didn't need to be rerun and all succeeded.

Summary: There was 1 WARNING message shown.

real    26m44.392s
user    0m15.428s
sys     0m4.708s
```


## Build an SD card image
```
[@sdk-container:/ ]$
time devtool build-image
```

You should see:

```
[@sdk-container:/ ]$
NOTE: Starting bitbake server...
NOTE: Reconnecting to bitbake server...
NOTE: Retrying server connection (#1)...
Loading cache: 100% |##########################################################################################################################################################| Time: 0:00:00
Loaded 3750 entries from dependency cache.
Parsing recipes: 100% |########################################################################################################################################################| Time: 0:00:05
Parsing of 2432 .bb files complete (2430 cached, 2 parsed). 3751 targets, 143 skipped, 0 masked, 0 errors.
WARNING: Skipping recipe linux-yocto-custom as it doesn't produce a package with the same name
WARNING: No recipes in workspace, building image core-image-minimal unmodified
Loading cache: 100% |##########################################################################################################################################################| Time: 0:00:19
Loaded 3750 entries from dependency cache.
Parsing recipes: 100% |########################################################################################################################################################| Time: 0:00:00
Parsing of 2432 .bb files complete (2430 cached, 2 parsed). 3751 targets, 143 skipped, 0 masked, 0 errors.
NOTE: Resolving any missing task queue dependencies
Initialising tasks: 100% |#####################################################################################################################################################| Time: 0:00:51
Checking sstate mirror object availability: 100% |#############################################################################################################################| Time: 0:00:02
Sstate summary: Wanted 137 Local 1 Network 0 Missed 136 Current 1259 (0% match, 90% complete) 
WARNING: The libxkbcommon:do_write_config sig is computed to be 712058bcc32c06dbf75bfc885cd5fe5aacbffe9fa535c0ba51606677e9d0c575, but the sig is locked to 42f1ac4dec2515b5153820afbf59e02d738c169435656443d8b633acb7c7f6cf in SIGGEN_LOCKEDSIGS_t-armv7at2hf-neon
The libepoxy:do_write_config sig is computed to be b5d02d3fa3cec67eee4c89e63d6df4583673d0b9392f7d1b015a872878daf2e4, but the sig is locked to 76748f010b30ef2540aaa657c2055e222f53a16d058eb895fa76e4609330658b in SIGGEN_LOCKEDSIGS_t-armv7at2hf-neon
The libgcc:do_fetch sig is computed to be 6b78d2eb3867ea973e9c7eee92034a89526765b5be364438f13c7a4773a227a0, but the sig is locked to ac749dad0603c033af49a4034733385550f5f46ceba3475b31221d1f93a67487 in SIGGEN_LOCKEDSIGS_t-armv7at2hf-neon
...
The adwaita-icon-theme:do_populate_sysroot sig is computed to be 98bd40f5e1b76259cee36dac6799768a761ca3b1e66f00e378259a4767b5514c, but the sig is locked to 2796debd0a9427f96cd00b4656045f7b2d21fad9b20c970dbffd889b0d15340d in SIGGEN_LOCKEDSIGS_t-allarch
The adwaita-icon-theme:do_packagedata sig is computed to be becbc688f70f16cf62c03d912a8b095fe22ec81d3f4b8342185d29dbe8d5e31f, but the sig is locked to 5b2f34916c6d739b05cda2060cc2cd6c8f4a716a93a4e67ec308d3287dad7fe6 in SIGGEN_LOCKEDSIGS_t-allarch
Removing 2 stale sstate objects for arch imx6q_phytec_mira_rdk_nand: 100% |####################################################################################################| Time: 0:00:00
NOTE: Executing Tasks
NOTE: Tasks Summary: Attempted 3802 tasks of which 3486 didn't need to be rerun and all succeeded.

Summary: There was 1 WARNING message shown.
INFO: Successfully built core-image-minimal. You can find output files in /opt/resy/imx6q-phytec-mira-rdk-nand/esdk/tmp/deploy/images/imx6q-phytec-mira-rdk-nand

real    6m13.702s
user    0m31.620s
sys     0m13.288s
```

<!-- ## Get U-Boot

We need to a little hack here (until/if ever the u-boot/hss stuff will be sorted). 
We need to build u-boot manually, since otherwise some files (e.g. boot.scr.uimg) required to built the image are not available where they should be.

```
[@sdk-container:/ ]$
devtool modify u-boot
```

You should see:

```
[@sdk-container:/ ]$
NOTE: Starting bitbake server...
NOTE: Reconnecting to bitbake server...
NOTE: Retrying server connection (#1)...
Loading cache: 100% |##########################################################################################################################################################| Time: 0:00:00
Loaded 3675 entries from dependency cache.
Parsing recipes: 100% |########################################################################################################################################################| Time: 0:00:00
Parsing of 2359 .bb files complete (2357 cached, 2 parsed). 3677 targets, 359 skipped, 1 masked, 0 errors.
INFO: SRC_URI contains some conditional appends/prepends - will create branches to represent these
NOTE: Resolving any missing task queue dependencies
Initialising tasks: 100% |#####################################################################################################################################################| Time: 0:00:00
Sstate summary: Wanted 0 Local 0 Network 0 Missed 0 Current 20 (0% match, 100% complete)
NOTE: Executing Tasks
NOTE: Tasks Summary: Attempted 93 tasks of which 90 didn't need to be rerun and all succeeded.
INFO: Adding local source files to srctree...
INFO: Source tree extracted to /opt/resy/riscv/workspace/sources/u-boot
WARNING: SRC_URI is conditionally overridden in this recipe, thus several devtool-override-* branches have been created, one for each override that makes changes to SRC_URI. It is recommended that you make changes to the devtool branch first, then checkout and rebase each devtool-override-* branch and update any unique patches there (duplicates on those branches will be ignored by devtool finish/update-recipe)
INFO: Recipe u-boot now set up to build from /opt/resy/riscv/workspace/sources/u-boot
[ sdk@146328dd9021:/workdir/build/m100pfsevp-polarfire-resy-master/tmp/deploy/sdk ]$ 
```

## Build U-Boot

```
[@sdk-container:/ ]$
devtool build u-boot
```

You should see:

```
[@sdk-container:/ ]$
NOTE: Starting bitbake server...
NOTE: Reconnecting to bitbake server...
NOTE: Retrying server connection (#1)...
Loading cache: 100% |##########################################################################################################################################################| Time: 0:00:00
Loaded 3675 entries from dependency cache.
Parsing recipes: 100% |########################################################################################################################################################| Time: 0:00:00
Parsing of 2359 .bb files complete (2356 cached, 3 parsed). 3677 targets, 359 skipped, 1 masked, 0 errors.
Loading cache: 100% |##########################################################################################################################################################| Time: 0:00:05
Loaded 3675 entries from dependency cache.
Parsing recipes: 100% |########################################################################################################################################################| Time: 0:00:00
Parsing of 2359 .bb files complete (2356 cached, 3 parsed). 3677 targets, 359 skipped, 1 masked, 0 errors.
NOTE: Resolving any missing task queue dependencies
Initialising tasks: 100% |#####################################################################################################################################################| Time: 0:00:01
Sstate summary: Wanted 8 Local 1 Network 0 Missed 7 Current 126 (12% match, 94% complete)
NOTE: Executing Tasks
NOTE: u-boot: compiling from external source tree /opt/resy/riscv/workspace/sources/u-boot
NOTE: Tasks Summary: Attempted 582 tasks of which 568 didn't need to be rerun and all succeeded.
```
7
## Build the Image

```
[@sdk-container:/ ]$
devtool build-image
```

You should see:

```
[@sdk-container:/ ]$
NOTE: Starting bitbake server...
NOTE: Reconnecting to bitbake server...
NOTE: Retrying server connection (#1)...
Loading cache: 100% |##########################################################################################################################################################| Time: 0:00:00
Loaded 3675 entries from dependency cache.
Parsing recipes: 100% |########################################################################################################################################################| Time: 0:00:00
Parsing of 2359 .bb files complete (2356 cached, 3 parsed). 3677 targets, 359 skipped, 1 masked, 0 errors.
INFO: Building image core-image-minimal-base with the following additional packages: u-boot
Loading cache: 100% |##########################################################################################################################################################| Time: 0:00:06
Loaded 3675 entries from dependency cache.
Parsing recipes: 100% |########################################################################################################################################################| Time: 0:00:00
Parsing of 2359 .bb files complete (2355 cached, 4 parsed). 3677 targets, 359 skipped, 1 masked, 0 errors.
NOTE: Resolving any missing task queue dependencies
Initialising tasks: 100% |#####################################################################################################################################################| Time: 0:00:06
Checking sstate mirror object availability: 100% |#############################################################################################################################| Time: 0:00:00
Sstate summary: Wanted 121 Local 1 Network 0 Missed 120 Current 981 (0% match, 89% complete)
WARNING: The os-release:do_compile sig is computed to be 0f9b29da00aa733c5b4592e188288cc93a52e31658c7cf0cb491fabc238ff7ac, but the sig is locked to 837b91781d58754a1efbdda8988c97929bb88e5dd6ea21de52192924b65df253 in SIGGEN_LOCKEDSIGS_t-allarch
The base-files:do_install sig is computed to be 49a2b510adc1b7ff48eddd35aa30ca2bffaac51cc73bd2eda2d2496ff0dd5882, but the sig is locked to 429561f32b726e18338bed4365ca6981578d3718e0b1e840090d8eac7e6bc157 in SIGGEN_LOCKEDSIGS_t-m100pfsevp
Removing 3 stale sstate objects for arch riscv64: 100% |#######################################################################################################################| Time: 0:00:00
Removing 2 stale sstate objects for arch m100pfsevp: 100% |####################################################################################################################| Time: 0:00:00
NOTE: Executing Tasks
NOTE: Tasks Summary: Attempted 2899 tasks of which 2870 didn't need to be rerun and all succeeded.

Summary: There was 1 WARNING message shown.
INFO: Successfully built core-image-minimal-base. You can find output files in /opt/resy/riscv/tmp/deploy/images/m100pfsevp
[ sdk@146328dd9021:/workdir/build/m100pfsevp-polarfire-resy-master/tmp/deploy/sdk ]$
``` -->

## Flash the Image to the SD Card

### Set up the `eSDK` environment on the buildhost

I guess it would be a good idea to do this on the buildhost and not in the SDK container. 
You can use the `bmaptool`, which comes with the `eSDK`.

```
[@buildhost:]
source /opt/resy/imx6q-phytec-mira-rdk-nand/buildtools-extended/environment-setup-x86_64-resysdk-linux
```

You should see something like this:
```
[@buildhost:]
Icecc not found. Disabling distributed compiling
```


```
[@buildhost:]
source /opt/resy/imx6q-phytec-mira-rdk-nand/esdk/environment-setup-armv7at2hf-neon-resy-linux-gnueabi
```

You should see something like this:

```
[@buildhost:]
Icecc not found. Disabling distributed compiling
SDK environment now set up; additionally you may now run devtool to perform development tasks.
Run devtool --help for further details.
```

### Flash the SD card on the buildhost

Plug the SD card into your SD card reader/writer

```
[@buildhost:]
cd /opt/resy/imx6q-phytec-mira-rdk-nand/esdk/tmp/deploy/images/imx6q-phytec-mira-rdk-nand/
sudo env "PATH=$PATH" bmaptool copy core-image-minimal-imx6q-phytec-mira-rdk-nand.wic.xz /dev/<your SD card reader/writer>
```

you should see something like this:
```
[@buildhost:]
bmaptool: info: discovered bmap file 'core-image-minimal-imx6q-phytec-mira-rdk-nand.wic.bmap'
bmaptool: info: block map format version 2.0
bmaptool: info: 593920 blocks of size 4096 (2.3 GiB), mapped 35992 blocks (140.6 MiB or 6.1%)
bmaptool: info: copying image 'core-image-minimal-imx6q-phytec-mira-rdk-nand.wic.xz' to block device '/dev/sdb' using bmap file 'core-image-minimal-base-imx6q-phytec-mira-rdk-nand.wic.bmap'
bmaptool: info: 100% copied
bmaptool: info: synchronizing '/dev/sdb'
bmaptool: info: copying time: 30.5s, copying speed 4.6 MiB/sec
```

## Boot it

- Plug the SD card into the target
- You might need to adjust some variable in the u-boot environment
- make sure to boot with `=> run uenvcmd_mmc_all`
- inpect the serial console(s) of the board

```
[@buildhost:serial console to target]
U-Boot SPL 2021.07 (Jul 05 2021 - 15:11:28 +0000)
Boot device 1
Trying to boot from MMC1
mmc_load_image_raw_sector: mmc block read error


U-Boot 2021.07 (Jul 05 2021 - 15:11:28 +0000)

CPU:   Freescale i.MX6Q rev1.3 at 792 MHz
Reset cause: POR
Model: PHYTEC phyBOARD-Mira Quad Carrier-Board with NAND
DRAM:  1 GiB
NAND:  1024 MiB
MMC:   FSL_SDHC: 0
Loading Environment from SPIFlash... SF: Detected n25q128a13 with page size 256 Bytes, erase size 4 KiB, total 16 MiB
OK
In:    serial@21e8000
Out:   serial@21e8000
Err:   serial@21e8000
Model: PHYTEC phyBOARD-Mira Quad Carrier-Board with NAND
Net:   eth0: ethernet@2188000
Hit any key to stop autoboot:  0
```

stop u-boot and run specific boot sequence:
```
[@buildhost:serial console to target]
=> run uenvcmd_mmc_all
```

you should see something like this:
```
[@buildhost:serial console to target]
bootfile: /boot/uImage
fdtfile: /boot/imx6q-phytec-mira-rdk-nand.dtb
ext4load mmc 0:1 0x10007fc0 /boot/uImage
10768960 bytes read in 594 ms (17.3 MiB/s)
ext4load mmc 0:1 0x18000000 /boot/imx6q-phytec-mira-rdk-nand.dtb
41325 bytes read in 22 ms (1.8 MiB/s) 
Booting from mmc - uEnv.txt from ext4...
bootargs=console=ttymxc1,115200n8 root=/dev/mmcblk0p1 rw rootfstype=ext4 rootwait noinitrd nohlt panic=1
## Booting kernel from Legacy Image at 10007fc0 ...
   Image Name:   Linux-5.10.27-std
   Image Type:   ARM Linux Kernel Image (no loading done) (uncompressed)
   Data Size:    10768896 Bytes = 10.3 MiB
   Load Address: 00000000
   Entry Point:  00000000
   Verifying Checksum ... OK
## Flattened Device Tree blob at 18000000
   Booting using the fdt blob at 0x18000000
   XIP Kernel Image (no loading done) 
   Using Device Tree in place at 18000000, end 1800d16c

Starting kernel ...

[    0.000000] Booting Linux on physical CPU 0x0
[    0.000000] Linux version 5.10.27-std (oe-user@oe-host) (arm-resy-linux-gnueabi-gcc (GCC) 11.1.1 20210523, GNU ld (GNU Binutils) 2.36.1.20210209) #1 SMP Tue Mar 30 12:32:09 UTC 2021
[    0.000000] CPU: ARMv7 Processor [412fc09a] revision 10 (ARMv7), cr=10c5387d
[    0.000000] CPU: PIPT / VIPT nonaliasing data cache, VIPT aliasing instruction cache
[    0.000000] OF: fdt: Machine model: PHYTEC phyBOARD-Mira Quad Carrier-Board with NAND
[    0.000000] Memory policy: Data cache writealloc
[    0.000000] efi: UEFI not found.   
[    0.000000] cma: Reserved 64 MiB at 0x4c000000
[    0.000000] Zone ranges:
[    0.000000]   DMA      [mem 0x0000000010000000-0x000000003fffffff]
[    0.000000]   Normal   empty
[    0.000000]   HighMem  [mem 0x0000000040000000-0x000000004fffffff]
[    0.000000] Movable zone start for each node
[    0.000000] Early memory node ranges
[    0.000000]   node   0: [mem 0x0000000010000000-0x000000004fffffff]
[    0.000000] Initmem setup node 0 [mem 0x0000000010000000-0x000000004fffffff]
[    0.000000] percpu: Embedded 20 pages/cpu s52300 r8192 d21428 u81920
[    0.000000] Built 1 zonelists, mobility grouping on.  Total pages: 260416
[    0.000000] Kernel command line: console=ttymxc1,115200n8 root=/dev/mmcblk0p1 rw rootfstype=ext4 rootwait noinitrd nohlt panic=1
[    0.000000] Dentry cache hash table entries: 131072 (order: 7, 524288 bytes, linear)
[    0.000000] Inode-cache hash table entries: 65536 (order: 6, 262144 bytes, linear)
[    0.000000] mem auto-init: stack:off, heap alloc:off, heap free:off
...
[    8.902186] random: 2 urandom warning(s) missed due to ratelimiting
[    9.640953] Micrel KSZ9031 Gigabit PHY 2188000.ethernet-1:03: attached PHY driver [Micrel KSZ9031 Gigabit PHY] (mii_bus:phy_addr=2188000.ethernet-1:03, irq=POLL)
[   19.127571] fec 2188000.ethernet eth0: Link is Up - 1Gbps/Full - flow control off
[   19.135653] IPv6: ADDRCONF(NETDEV_CHANGE): eth0: link becomes ready

Resy (Reliable Embedded Systems Reference Distro) 3.3+snapshot-5dce2f3da20a14c0eb5229696561b0c5f6fce54c imx6q-phytec-mira-rdk-nand /dev/ttymxc1

imx6q-phytec-mira-rdk-nand login: root
```
As you see above you can log in as `root` without a password.

Let's check the timestamp:
```
[@buildhost:serial console to target]
root@imx6q-phytec-mira-rdk-nand:~# cat /etc/timestamp 
20210724150911
```
If this is the same timestamp as you see on your image file we are good.