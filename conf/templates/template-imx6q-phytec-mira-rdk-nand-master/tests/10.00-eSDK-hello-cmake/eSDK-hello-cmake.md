# Objectives

Get/Build/Deploy `hellocmake`[1]

[1] https://gitlab.com/exempli-gratia/hellocmake.git

# Minimal build framework

@@@ see:

# Install the eSDK

@@@ see:

# `hellocmake` with the eSDK

Get into the SDK container:

```
[@buildhost:]
cd /workdir
./resy-sdk-container.sh
```

## Setup buildtools extended environment (each time)

setup the environment for buildtools-extended:
```
[@sdk-container:/ ]$
source /opt/resy/imx6q-phytec-mira-rdk-nand/buildtools-extended/environment-setup-x86_64-resysdk-linux
```

you should see something like this:
```
[@sdk-container:/ ]$
Icecc not found. Disabling distributed compiling
```

## Setup SDK environment in SDK container (each time)

Setup the SDK environment in the container:

```
[@sdk-container:/ ]$
source /opt/resy/imx6q-phytec-mira-rdk-nand/esdk/environment-setup-armv7at2hf-neon-resy-linux-gnueabi
```

You should see:

```
[@sdk-container:/ ]$
Icecc not found. Disabling distributed compiling
SDK environment now set up; additionally you may now run devtool to perform development tasks.
Run devtool --help for further details.
```

## `devtool add`

```
[@sdk-container:/ ]$
devtool add hellocmake https://gitlab.com/exempli-gratia/hellocmake.git
```

You should see:

```
[@sdk-container:/ ]$
NOTE: Starting bitbake server...
NOTE: Starting bitbake server...
INFO: Fetching git://gitlab.com/exempli-gratia/hellocmake.git;protocol=https...
Loading cache: 100% |##########################################################################################################################################################| Time: 0:00:10
Loaded 3750 entries from dependency cache.
Parsing recipes: 100% |########################################################################################################################################################| Time: 0:00:11
Parsing of 2433 .bb files complete (2430 cached, 3 parsed). 3752 targets, 143 skipped, 0 masked, 0 errors.
NOTE: Resolving any missing task queue dependencies
Initialising tasks: 100% |#####################################################################################################################################################| Time: 0:00:00
Sstate summary: Wanted 0 Local 0 Network 0 Missed 0 Current 0 (0% match, 0% complete)
NOTE: No setscene tasks
NOTE: Executing Tasks
NOTE: Tasks Summary: Attempted 2 tasks of which 0 didn't need to be rerun and all succeeded.
INFO: Using default source tree path /opt/resy/imx6q-phytec-mira-rdk-nand/esdk/workspace/sources/hellocmake
NOTE: Reconnecting to bitbake server...
NOTE: Retrying server connection (#1)...
NOTE: Reconnecting to bitbake server...
NOTE: Reconnecting to bitbake server...
NOTE: Retrying server connection (#1)...
NOTE: Retrying server connection (#1)...
NOTE: Starting bitbake server...
INFO: Recipe /opt/resy/imx6q-phytec-mira-rdk-nand/esdk/workspace/recipes/hellocmake/hellocmake_git.bb has been automatically created; further editing may be required to make it fully functional
```

## `devtool edit-recipe`

```
[@sdk-container:/ ]$
devtool edit-recipe hellocmake
```

You should see `vim` opening this `recipe`:

```
[@sdk-container:/ ]$
# Recipe created by recipetool
# This is the basis of a recipe and may need further editing in order to be fully functional.
# (Feel free to remove these comments when editing.)

# WARNING: the following LICENSE and LIC_FILES_CHKSUM values are best guesses - it is
# your responsibility to verify that the values are complete and correct.
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = "git://gitlab.com/exempli-gratia/hellocmake.git;protocol=https"

# Modify these as desired
PV = "1.0+git${SRCPV}"
SRCREV = "d287d38ab4da45130d332ed332ab6a58fa92ba18"

S = "${WORKDIR}/git"

inherit cmake

# Specify any options you want to pass to cmake using EXTRA_OECMAKE:
EXTRA_OECMAKE = ""
```

exit `vim` with `:q!`

## `devtool build`

```
[@sdk-container:/ ]$
devtool build hellocmake
```

You should see:

```
[@sdk-container:/ ]$
NOTE: Starting bitbake server...
NOTE: Reconnecting to bitbake server...
NOTE: Retrying server connection (#1)...
Loading cache: 100% |##########################################################################################################################################################| Time: 0:00:01
Loaded 3750 entries from dependency cache.
Parsing recipes: 100% |########################################################################################################################################################| Time: 0:00:05
Parsing of 2433 .bb files complete (2430 cached, 3 parsed). 3752 targets, 143 skipped, 0 masked, 0 errors.
Loading cache: 100% |##########################################################################################################################################################| Time: 0:00:18
Loaded 3750 entries from dependency cache.
Parsing recipes: 100% |########################################################################################################################################################| Time: 0:00:00
Parsing of 2433 .bb files complete (2430 cached, 3 parsed). 3752 targets, 143 skipped, 0 masked, 0 errors.
NOTE: Resolving any missing task queue dependencies
Initialising tasks: 100% |#####################################################################################################################################################| Time: 0:00:02
Sstate summary: Wanted 8 Local 1 Network 0 Missed 7 Current 140 (12% match, 95% complete)
WARNING: The gcc-runtime:do_fetch sig is computed to be 6c43dffd4299d31d8ddf23d75cedbbe1d60346f18a4a80990e517a227742e4f2, but the sig is locked to 5e3056930aa8620edd761037730baaae39daf0f2d22eee70aaa5aa5fc87dde55 in SIGGEN_LOCKEDSIGS_t-armv7at2hf-neon
The libgcc:do_deploy_source_date_epoch sig is computed to be f5f35265f5a65a795cbe4105638ded374396b6f1990ef4b2fe7f87f1dba24022, but the sig is locked to 813c96e4a6bdc5b9cdfec9b63153ab1f3838a34cdbfab0ba63f091149c1daca8 in SIGGEN_LOCKEDSIGS_t-armv7at2hf-neon
...
The gcc-runtime:do_packagedata sig is computed to be 44b089c56452aebc212e1f9372426454afc82a78bcc7118cb087f91c314b1d14, but the sig is locked to e3fcef5f9d2620380b82f80a3ae878b71c974ad8f228c011b1bc969e70dd4302 in SIGGEN_LOCKEDSIGS_t-armv7at2hf-neon
The gcc-runtime:do_populate_sysroot sig is computed to be b8a7e40d4256efa7f800bb42b14daefe2b9ef0848c9ace1cd0096db489c8a2bc, but the sig is locked to abdf315c56510ccb9fdef94bbf1517828cb6065fcd86317f0f1f777c2bc6af3a in SIGGEN_LOCKEDSIGS_t-armv7at2hf-neon
NOTE: Executing Tasks
NOTE: hellocmake: compiling from external source tree /opt/resy/imx6q-phytec-mira-rdk-nand/esdk/workspace/sources/hellocmake
NOTE: Tasks Summary: Attempted 640 tasks of which 631 didn't need to be rerun and all succeeded.

Summary: There was 1 WARNING message shown.
```

## `devtool deploy-target`

Make sure you can connect via `ssh` to your target.
```
[@buildhost:]
ssh root@<target-ip>
```

You might need to fix some things like
```
[@buildhost:]
ssh-keygen -f "/home/<user>/.ssh/known_hosts" -R <target-ip>
```

try again:
```
[@buildhost:]
ssh root@<target-ip>
```

Now you should be able to log in to the target.


```
[@sdk-container:/ ]$
devtool deploy-target hellocmake root@<target-ip>
```

You should see:

```
[@sdk-container:/ ]$
NOTE: Starting bitbake server...
NOTE: Reconnecting to bitbake server...
NOTE: Retrying server connection (#1)...
Loading cache: 100% |##########################################################################################################################################################| Time: 0:00:00
Loaded 3750 entries from dependency cache.
Parsing recipes: 100% |########################################################################################################################################################| Time: 0:00:05
Parsing of 2433 .bb files complete (2430 cached, 3 parsed). 3752 targets, 143 skipped, 0 masked, 0 errors.
INFO: Successfully deployed /opt/resy/imx6q-phytec-mira-rdk-nand/esdk/tmp/work/armv7at2hf-neon-resy-linux-gnueabi/hellocmake/1.0+git999-r0/image
```

## run it on the target
```
ssh root@<target-ip>
which hellocmake
```

you should see:

```
[@buildhost:ssh session]
/usr/bin/hellocmake
```

```
[@buildhost:ssh session]
hellocmake
```

you should see something like this:
```
[@buildhost:ssh session]
Hello World CMake from git!
```

(Optional) - build an image

## Build the Image

```
[@sdk-container:/ ]$
devtool build-image
```

## Flash the Image to the SD Card

See eSDK-install

## Run the Image

See eSDK-install