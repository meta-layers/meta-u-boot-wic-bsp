# Objectives

- Build/Deploy original `imx6q-phytec-mira-rdk-nand.dtb`
- Create/Build/Deploy modified `imx6q-phytec-mira-rdk-nand-modified.dtb`

# Minimal build framework

@@@ see:

# Install the eSDK

@@@ see:

# device tree with the eSDK

Get into the SDK container:

```
[@buildhost:]
cd /workdir
./resy-sdk-container.sh
```

## Setup buildtools extended environment (each time)

setup the environment for buildtools-extended:
```
[@sdk-container:/ ]$
source /opt/resy/imx6q-phytec-mira-rdk-nand/buildtools-extended/environment-setup-x86_64-resysdk-linux
```

you should see something like this:
```
[@sdk-container:/ ]$
Icecc not found. Disabling distributed compiling
```

## Setup SDK environment in SDK container (each time)

Setup the SDK environment in the container:

```
[@sdk-container:/ ]$
source /opt/resy/imx6q-phytec-mira-rdk-nand/esdk/environment-setup-armv7at2hf-neon-resy-linux-gnueabi
```

You should see:

```
[@sdk-container:/ ]$
Icecc not found. Disabling distributed compiling
SDK environment now set up; additionally you may now run devtool to perform development tasks.
Run devtool --help for further details.
```

# Get/Build/Deploy original `imx6q-phytec-mira-rdk-nand.dtb`

## build it the classic way without `devtool`

```
[@sdk-container:/ ]$
cd /opt/resy/imx6q-phytec-mira-rdk-nand/esdk/tmp/work/imx6q_phytec_mira_rdk_nand-resy-linux-gnueabi/linux-yocto-custom/5.10.27-std+git999-r0/linux-yocto-custom-5.10.27-std+git999//
make imx6q-phytec-mira-rdk-nand.dtb
```

You should see:

```
[@sdk-container:/ ]$
  SYNC    include/config/auto.conf.cmd
  GEN     Makefile
  HOSTCC  scripts/basic/fixdep
  HOSTCC  scripts/kconfig/conf.o
  HOSTCC  scripts/kconfig/confdata.o
  HOSTCC  scripts/kconfig/expr.o
  HOSTCC  scripts/kconfig/lexer.lex.o
  HOSTCC  scripts/kconfig/parser.tab.o
  HOSTCC  scripts/kconfig/preprocess.o
  HOSTCC  scripts/kconfig/symbol.o
  HOSTCC  scripts/kconfig/util.o
  HOSTLD  scripts/kconfig/conf
  HOSTCC  scripts/dtc/dtc.o
  HOSTCC  scripts/dtc/flattree.o
  HOSTCC  scripts/dtc/fstree.o
  HOSTCC  scripts/dtc/data.o
  HOSTCC  scripts/dtc/livetree.o
  HOSTCC  scripts/dtc/treesource.o
  HOSTCC  scripts/dtc/srcpos.o
  HOSTCC  scripts/dtc/checks.o
  HOSTCC  scripts/dtc/util.o
  HOSTCC  scripts/dtc/dtc-lexer.lex.o
  HOSTCC  scripts/dtc/dtc-parser.tab.o
  HOSTLD  scripts/dtc/dtc
  DTC     arch/arm/boot/dts/imx6q-phytec-mira-rdk-nand.dtb
```

On this board `/boot/imx6q-phytec-mira-rdk-nand.dtb` is the device tree being used.

Let's copy over our manually built device tree and see if the board still boots:

```
[@sdk-container:/ ]$
scp /opt/resy/imx6q-phytec-mira-rdk-nand/esdk/tmp/work/imx6q_phytec_mira_rdk_nand-resy-linux-gnueabi/linux-yocto-custom/5.10.27-std+git999-r0/linux-yocto-custom-5.10.27-std+git999/arch/arm/boot/dts/imx6q-phytec-mira-rdk-nand.dtb root@<target-ip>:/boot/imx6q-phytec-mira-rdk-nand.dtb.manually
```

Let's fix up some things on the target to use it:

```
[@buildhost:serial-console]
cd /boot
cp imx6q-phytec-mira-rdk-nand.dtb imx6q-phytec-mira-rdk-nand.dtb.ori
ln -sf imx6q-phytec-mira-rdk-nand.dtb.manually imx6q-phytec-mira-rdk-nand.dtb
ls -lah imx6q-phytec-mira-rdk-nand.dtb
```

You should see:

```
[@buildhost:serial-console]
lrwxrwxrwx 1 root root 39 Jul 26 10:03 imx6q-phytec-mira-rdk-nand.dtb -> imx6q-phytec-mira-rdk-nand.dtb.manually
```

Reboot the target and see if it still boots.

In my case it does.

# Get/Build/Deploy modified `imx6q-phytec-mira-rdk-nand.dtb`

We'll just rename it and add a `.dtsi` file which should show up in `/proc/device-tree`.

## get the `.dtsi` file, rename the device tree, include the `.dtsi` file and build it

```
[@sdk-container:/ ]$
# get into the kernel sources where device trees are:
pushd /opt/resy/imx6q-phytec-mira-rdk-nand/esdk/workspace/sources/linux-yocto-custom/arch/arm/boot/dts
# get the .dtsi file
wget https://gitlab.com/exempli-gratia/devicetreedummy/-/raw/master/dummy.dtsi
# rename the .dts file
cp imx6q-phytec-mira-rdk-nand.dts imx6q-phytec-mira-rdk-nand-modified.dts
# include the .dtsi file in the new dts.file
vim imx6q-phytec-mira-rdk-nand-modified.dts
```

Include `dummy.dtsi` like this:

```
[@sdk-container:/ ]$
// SPDX-License-Identifier: (GPL-2.0+ OR MIT)
/*
 * Copyright (C) 2018 PHYTEC Messtechnik GmbH
 * Author: Christian Hemp <c.hemp@phytec.de>
 */

/dts-v1/;
#include "imx6q.dtsi"
#include "imx6qdl-phytec-phycore-som.dtsi"
#include "imx6qdl-phytec-mira.dtsi"
#include "dummy.dtsi"

/ {
```

```
[@sdk-container:/ ]$
# write and exit vim
ESC:wq!
# return to where you were before
popd
```

## build it the classic way without `devtool`

```
[@sdk-container:/ ]$
# optional
cd /opt/resy/imx6q-phytec-mira-rdk-nand/esdk/tmp/work/imx6q_phytec_mira_rdk_nand-resy-linux-gnueabi/linux-yocto-custom/5.10.27-std+git999-r0/linux-yocto-custom-5.10.27-std+git999//
# build it:
make imx6q-phytec-mira-rdk-nand-modified.dtb
```

You should see:

```
[@sdk-container:/ ]$
    DTC     arch/arm/boot/dts/imx6q-phytec-mira-rdk-nand-modified.dtb
```

On this board `/boot/imx6q-phytec-mira-rdk-nand.dtb` is the device tree being used.

Let's copy over our manually built/modified device tree and see if the board still boots:

```
[@sdk-container:/ ]$
scp /opt/resy/imx6q-phytec-mira-rdk-nand/esdk/tmp/work/imx6q_phytec_mira_rdk_nand-resy-linux-gnueabi/linux-yocto-custom/5.10.27-std+git999-r0/linux-yocto-custom-5.10.27-std+git999/arch/arm/boot/dts/imx6q-phytec-mira-rdk-nand-modified.dtb root@<target-ip>:/boot/imx6q-phytec-mira-rdk-nand.dtb.modified
```

Let's fix up some things on the target to use it:

```
[@buildhost:serial-console]
cd /boot
ln -sf imx6q-phytec-mira-rdk-nand.dtb.modified imx6q-phytec-mira-rdk-nand.dtb
ls -lah imx6q-phytec-mira-rdk-nand.dtb
```

You should see:

```
[@buildhost:serial-console]
lrwxrwxrwx 1 root root 39 Jul 26 10:31 imx6q-phytec-mira-rdk-nand.dtb -> imx6q-phytec-mira-rdk-nand.dtb.modified
```

Reboot the target and see if it still boots.

In my case it does.

Now let's try to find the new device tree node:

```
[@buildhost:serial-console]
cd /proc/device-tree/fakedummy
ls -lah
```

You should see:

```
[@buildhost:serial-console]
total 0
drwxr-xr-x  2 root root  0 Jul 26 10:34 .
drwxr-xr-x 26 root root  0 Jul 26 10:34 ..
-r--r--r--  1 root root 11 Jul 26 10:34 compatible
-r--r--r--  1 root root  4 Jul 26 10:34 default-state
-r--r--r--  1 root root  4 Jul 26 10:34 myotherprop
-r--r--r--  1 root root 20 Jul 26 10:34 myproperty
-r--r--r--  1 root root 10 Jul 26 10:34 name
```

which pretty much corresponds to our `.dtsi` file:

```
/*
 * Sample DTSI file.
 * (c)  Jan-Simon Moeller (jsmoeller@linuxfoundation.org)
 *
 */
/ {

        fakedummy {
                compatible = "fake,dummy";

                myproperty  = "this is my property";
                myotherprop = <2>;
                default-state = "off";
        };

};
```

(Optional) - build an image

## Build the Image

```
[@sdk-container:/ ]$
devtool build-image
```

## Flash the Image to the SD Card

See eSDK-install

## Run the Image

See eSDK-install