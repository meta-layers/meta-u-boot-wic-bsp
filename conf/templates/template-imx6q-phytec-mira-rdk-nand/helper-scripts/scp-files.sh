#!/bin/bash

pushd /workdir/build/imx6q-phytec-mira-rdk-nand-wic/tmp/deploy/images/imx6q-phytec-mira-rdk-nand

core-image-minimal-imx6q-phytec-mira-rdk-nand.rootfs-20240330082111.tar.xz

SOURCE_1="core-image-minimal-imx6q-phytec-mira-rdk-nand.rootfs-*.wic*"

TARGET_1="student@192.168.42.60:/home/student/projects/imx6q-phytec-mira-rdk-nand-wic/tmp/deploy/images/imx6q-phytec-mira-rdk-nand"

set -x
scp -r ${SOURCE_1} ${TARGET_1}
set +x

popd
