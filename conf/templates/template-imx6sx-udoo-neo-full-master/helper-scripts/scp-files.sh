#!/bin/bash

DEPLOY="imx6sx-udoo-neo-full-wic-master/tmp/deploy/images/imx6sx-udoo-neo-full"

pushd /workdir/build/${DEPLOY}

SOURCE_1="core-image-minimal-imx6sx-udoo-neo-full-*.rootfs.wic*"

TARGET_1="student@192.168.42.60:/home/student/projects/${DEPLOY}"

set -x
scp -r ${SOURCE_1} ${TARGET_1}
set +x

popd
