#!/bin/bash

DEPLOY="stm32mp157c-dk2-wic-master/tmp/deploy/images/stm32mp157c-dk2"

pushd /workdir/build/${DEPLOY}

SOURCE_1="core-image-minimal-stm32mp157c-dk2-*.rootfs.wic*"
SOURCE_2="core-image-minimal-base-stm32mp157c-dk2-*.rootfs.wic*"

TARGET_1="student@192.168.42.60:/home/student/projects/${DEPLOY}"

set -x
scp -r ${SOURCE_1} ${TARGET_1}
scp -r ${SOURCE_2} ${TARGET_1}
set +x

popd
