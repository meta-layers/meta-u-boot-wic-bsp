# Copyright (C) 2021 Robert Berger <robert.berger@ReliableEmbeddedSystems.com>
# Released under the MIT license (see COPYING.MIT for the terms)

SUMMARY = "External device trees"
DESCRIPTION = "Device tree(s) external to kernel."
SECTION = "bsp"

# the device trees from within the layer are licensed as MIT, kernel includes are GPL
LICENSE = "MIT & GPLv2"
LIC_FILES_CHKSUM = " \
		file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302 \
		file://${COMMON_LICENSE_DIR}/GPL-2.0-only;md5=801f80980d171dd6425610833a22dbe6 \
		"

inherit devicetree
PROVIDES = "virtual/dtb"

# common include
SRC_URI:append = " file://dummy-addon-external.dtsi"

# device tree sources for the various machines
COMPATIBLE_MACHINE:imx6q-phytec-mira-rdk-nand = ".*"
SRC_URI:append:imx6q-phytec-mira-rdk-nand = " file://imx6q-phytec-mira-rdk-nand-addon-external.dts"
SRC_URI:append:imx6q-phytec-mira-rdk-nand = " file://imx6q-phytec-mira-rdk-nand-addon-external-chosen.dts"

# put the overlay to the end, otherwise it can not be applied over a device tree
# please note, that the kernel-fitimage.bbclass does not support overlays (if I understand this correctly)
SRC_URI:append = " file://overlay-dummy-external.dts"
