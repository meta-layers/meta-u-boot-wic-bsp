FILESEXTRAPATHS:prepend := "${THISDIR}/${MACHINE}:"
SUMMARY = "uboot.env helper"
SECTION = "bsp"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

# place different uboot.env files in different subdirs with MACHINE name
SRC_URI += " \
    file://uboot.env \
"

S = "${WORKDIR}/sources"
UNPACKDIR = "${S}"

do_install() {
	# place in rootfs
        install -d ${D}/${rootdir}/u-boot-env
        install -m 0444 ${UNPACKDIR}/uboot.env ${D}/${rootdir}/u-boot-env/uboot.env
}

FILES:${PN} += "/u-boot-env/uboot.env"

inherit deploy

## Copy script to the deploy area with u-boot, uImage, and rootfs
do_deploy() {
   install -d ${DEPLOY_DIR_IMAGE}
   install -m 0755 ${UNPACKDIR}/uboot.env ${DEPLOY_DIR_IMAGE}
}
addtask do_deploy after do_install

COMPATIBLE_MACHINE = "^("
COMPATIBLE_MACHINE .= "beagle-bone-black"
COMPATIBLE_MACHINE .= "|beagle-bone-black-conserver"
COMPATIBLE_MACHINE .= "|imx6ul-phytec-segin"
COMPATIBLE_MACHINE .= "|am335x-phytec-wega"
COMPATIBLE_MACHINE .= "|am335x-regor-rdk"
COMPATIBLE_MACHINE .= "|imx6sx-udoo-neo-full"
#COMPATIBLE_MACHINE .= "|phyboard-polis-imx8mm-4"
#COMPATIBLE_MACHINE .= "|phyboard-polis-imx8mm-4"
#COMPATIBLE_MACHINE .= "|phyboard-polis-imx8mn-1"
#COMPATIBLE_MACHINE .= "|phyboard-pollux-imx8mp-1"
#COMPATIBLE_MACHINE .= "|phygate-tauri-l-imx8mm-1"
#COMPATIBLE_MACHINE .= "|phygate-tauri-l-imx8mm-2"
COMPATIBLE_MACHINE .= ")$"
