setenv netdev eth0
setenv ethprime FEC1
setenv ethact FEC1
setenv ethaddr 50:2d:f4:0a:d9:64
setenv fec_addr 50:2d:f4:0a:d9:64
setenv ipaddr 192.168.42.85
setenv serverip 192.168.42.1
setenv gatewayip 192.168.42.254
saveenv
