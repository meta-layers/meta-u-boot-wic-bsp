FILESEXTRAPATHS:prepend := "${THISDIR}/${MACHINE}:"
SUMMARY = "u-boot uEnv.txt helper"
SECTION = "bsp"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

# eSDK: u-boot-uenv-helper_1.0.bb:do_fetch failed with exit code 'setscene whitelist'
#       but the problem is elsewhere - see all the way down

# place different uEnv.txt files in different subdirs with MACHINE name
SRC_URI = " \
    file://uEnv.txt \
"

# this is wrong:
#DEPENDS_imx6q-phytec-mira-rdk-nand += "u-boot"
#RDEPENDS:${PN}_imx6q-phytec-mira-rdk-nand += "u-boot"

# it should be:
do_install[depends] = "u-boot:do_deploy"

S = "${WORKDIR}/sources"
UNPACKDIR = "${S}"

do_install() {
	# place in rootfs
        install -d ${D}/${rootdir}
        install -m 0444 ${UNPACKDIR}/uEnv.txt ${D}/${rootdir}/uEnv.txt
}

do_install:append:imx6q-phytec-mira-rdk-nand() {
        # place in rootfs
        install -d ${D}/${rootdir}
        install -m 0666 ${DEPLOY_DIR_IMAGE}/u-boot.img ${D}/${rootdir}/u-boot.img 
	# this is a hack for now
        install -m 0666 ${DEPLOY_DIR_IMAGE}/u-boot.img ${D}/${rootdir}/u-boot-dtb.img
}

FILES:${PN} += "/uEnv.txt"

FILES:${PN}:imx6q-phytec-mira-rdk-nand += "/uEnv.txt \
                                           /u-boot.img \
                                           /u-boot-dtb.img \
                                           "
## eSDK: u-boot-uenv-helper_1.0.bb:do_fetch failed with exit code 'setscene whitelist'
## The problem is here!!!
## if you call something do_deploy and probably if you do the after
## do_install then do_deploy is called multiple times by eSDK build
## unless you inherit deploy?
## Copy script to the deploy area with u-boot, uImage, and rootfs

inherit deploy

do_deploy () {
   install -d ${DEPLOY_DIR_IMAGE}
   install -m 0755 ${UNPACKDIR}/uEnv.txt ${DEPLOY_DIR_IMAGE}
}
addtask do_deploy after do_install
