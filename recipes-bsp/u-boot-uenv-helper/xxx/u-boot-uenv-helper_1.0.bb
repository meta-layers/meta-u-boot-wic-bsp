FILESEXTRAPATHS_prepend := "${THISDIR}/${MACHINE}:"
SUMMARY = "u-boot uEnv.txt helper"
SECTION = "bsp"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

# eSDK: u-boot-uenv-helper_1.0.bb:do_fetch failed with exit code 'setscene whitelist'
# place different uEnv.txt files in different subdirs with MACHINE name
#SRC_URI = " \
#    file://uEnv.txt \
#"

SRC_URI = "git://gitlab.com/exempli-gratia/u-boot-uenv-helper.git;protocol=https"
#;subpath=${MACHINE_ARCH}"
SRCREV = "${AUTOREV}"
PV = "0.0+git${SRCPV}"

S = "${WORKDIR}/git"

# this is wrong:
#DEPENDS_imx6q-phytec-mira-rdk-nand += "u-boot"
#RDEPENDS_${PN}_imx6q-phytec-mira-rdk-nand += "u-boot"

# it should be:
do_install[depends] = "u-boot:do_deploy"

#S = "${WORKDIR}"

do_install() {
	# place in rootfs
        install -d ${D}/${rootdir}
        install -m 0444 ${S}/${MACHINE_ARCH}/uEnv.txt ${D}/${rootdir}/uEnv.txt
}

do_install_append_imx6q-phytec-mira-rdk-nand() {
        # place in rootfs
        install -d ${D}/${rootdir}
        install -m 0666 ${DEPLOY_DIR_IMAGE}/u-boot.img ${D}/${rootdir}/u-boot.img 
	# this is a hack for now
        install -m 0666 ${DEPLOY_DIR_IMAGE}/u-boot.img ${D}/${rootdir}/u-boot-dtb.img
}

FILES_${PN} = "/uEnv.txt"

FILES_${PN}_imx6q-phytec-mira-rdk-nand = "/uEnv.txt \
                                           /u-boot.img \
                                           /u-boot-dtb.img \
                                           "
## Copy script to the deploy area with u-boot, uImage, and rootfs
do_not_deploy () {
   install -d ${DEPLOY_DIR_IMAGE}
   install -m 0755 ${S}/${MACHINE_ARCH}/uEnv.txt ${DEPLOY_DIR_IMAGE}
}
addtask do_not_deploy after do_install
