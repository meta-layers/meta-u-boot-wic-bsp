### First time:
#
## reset u-boot env (if necessary):
# env default -a
#
# fatload mmc 0:1 0x3000000 uEnv.txt
# env import -t 0x3000000 ${filesize}
# run uenvcmd
#
## to make it permanent and autoboot:
# env default -a
#
# fatload mmc 0:2 0x18000000 uEnv.txt
# env import -t 0x18000000 ${filesize}
# setenv bootcmd 'run uenvcmd'
# saveenv
# reset

hostname=zedboard

kernel_addr_r=0x3000000
expand_bootfile=setenv bootfile ${hostname}/uImage

fdt_addr_r=0x2a00000
expand_fdtfile=setenv fdtfile ${hostname}/uImage-zynq-zed.dtb

bitstream_addr_r=0x100000
expand_bitstreamfile=setenv bitstreamfile ${hostname}/fpga.bin

ipaddr=192.168.42.11
serverip=192.168.42.1
gatewayip=192.168.42.1
netmask=255.255.255.0
# read from elsewhere 
#ethaddr=00:04:9f:13:57:b4
#netdev=eth0

fdt_high=0xffffffff

# --> uImage from tftp
kernel_netload=tftpboot ${kernel_addr_r} ${bootfile}
# <-- uImage from tftp

# --> fdt from tftp
fdt_netload=tftpboot ${fdt_addr_r} ${fdtfile}
# <-- fdt from tftp

# --> bitstream from tftp
bitstream_netload=tftpboot ${bitstream_addr_r} ${bitstreamfile}
# <-- bitstream from tftp

# --> uImage and fdt from tftp
netload=run kernel_netload fdt_netload
# <-- uImage and fdt from tftp

# --> load bitstream from tftp and into fpga
#bitstream_fpga_load=run bitstream_netload; fpga load 0 ${bitstream_addr_r} ${filesize}
bitstream_fpga_load=if run bitstream_netload; then fpga load 0 ${bitstream_addr_r} ${filesize}; else echo no ${bitstream}; fi
# <-- load bitstream from tftp and into fpga

# --> generic bootargs
# default:
bootargs=console=ttyPS0,115200n8
# bootgraph:
#bootargs=initcall_debug printk.time=y console=ttyO2,115200n8
# bootchart:
#bootargs=initcall_debug printk.time=y init=/sbin/bootchartd console=ttyO2,115200n8
# <-- generic bootargs

# --> rootfs from nfs
nfsroot=/opt/poky/zedboard-rootfs
ips_to_bootargs=setenv bootargs ${bootargs} ip=${ipaddr}:${serverip}:${gatewayip}:${netmask}:${hostname}:${netdev}:off
nfs_to_bootargs=setenv bootargs ${bootargs} nfsroot=${serverip}:${nfsroot},v3,tcp
default_to_bootargs=setenv bootargs ${bootargs} noinitrd nohlt panic=1 earlyprintk=ttyPS0,115200n8
compose_nfs_bootargs=run ips_to_bootargs; run nfs_to_bootargs; run default_to_bootargs
nfsboot=echo Booting from nfs...; run compose_nfs_bootargs

# --> bring up network
#netup=dcache off; usb start
#netup=usb start
# <-- bring up network

compose_nfsboot=run nfsboot
# <-- rootfs from nfs

compose_default=run expand_bootfile; echo bootfile: ${bootfile}; run expand_fdtfile; echo fdtfile: ${fdtfile}; run expand_bitstreamfile; echo bitstreamfile: ${bitstreamfile}; ping ${serverip}
uenvcmd=run compose_default; run netload; run compose_nfsboot; printe bootargs; bootm ${kernel_addr_r} - ${fdt_addr_r}
