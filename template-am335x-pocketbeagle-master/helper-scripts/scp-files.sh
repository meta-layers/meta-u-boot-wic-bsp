#!/bin/bash

DEPLOY="am335x-pocketbeagle-wic-master/tmp/deploy/images/am335x-pocketbeagle"

pushd /workdir/build/${DEPLOY}

SOURCE_1="core-image-minimal-am335x-pocketbeagle-*.rootfs.wic*"

TARGET_1="student@192.168.42.60:/home/student/projects/${DEPLOY}"

set -x
scp -r ${SOURCE_1} ${TARGET_1}
set +x

popd
