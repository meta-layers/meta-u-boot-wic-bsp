#!/bin/bash

pushd /workdir/build/beagle-bone-black-virt-wic/tmp/deploy/images/beagle-bone-black

SOURCE_1="core-image-minimal-beagle-bone-black-*.rootfs.wic*"
SOURCE_2="core-image-minimal-virt-docker-ce-beagle-bone-black-*.rootfs.wic*"


TARGET_1="student@192.168.42.60:/home/student/projects/beagle-bone-black-virt-wic/tmp/deploy/images/beagle-bone-black"

scp -r ${SOURCE_1} ${TARGET_1}
scp -r ${SOURCE_2} ${TARGET_1}

popd
