#!/bin/bash

pushd /workdir/build/beagle-bone-black-wic/tmp/deploy/images/beagle-bone-black

SOURCE_1="core-image-minimal-beagle-bone-black-*.rootfs.wic*"

TARGET_1="student@192.168.42.60:/home/student/projects/beagle-bone-black-wic/tmp/deploy/images/beagle-bone-black"

set -x
scp -r ${SOURCE_1} ${TARGET_1}
set +x

popd
