#!/bin/bash

pushd /workdir/build/beagle-bone-green-wic/tmp/deploy/images/beagle-bone-green

SOURCE_1="core-image-minimal-beagle-bone-green-*.rootfs.wic*"

TARGET_1="student@192.168.42.60:/home/student/projects/beagle-bone-green-wic/tmp/deploy/images/beagle-bone-green"

set -x
scp -r ${SOURCE_1} ${TARGET_1}
set +x

popd
