#!/bin/bash

DEPLOY="de0-nano-soc-kit-wic-master/tmp/deploy/images/de0-nano-soc-kit"

pushd /workdir/build/${DEPLOY}

SOURCE_1="core-image-minimal-de0-nano-soc-kit-*.rootfs.wic*"

TARGET_1="student@192.168.42.60:/home/student/projects/${DEPLOY}"

set -x
scp -r ${SOURCE_1} ${TARGET_1}
set +x

popd
