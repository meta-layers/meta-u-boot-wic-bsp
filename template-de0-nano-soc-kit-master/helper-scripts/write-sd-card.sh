DEVICE="/dev/sdc"
#IMAGE="core-image-minimal-imx6q-phytec-mira-rdk-nand-20200602111703.rootfs.wic"
#IMAGE="core-image-minimal-de0-nano-soc-kit-20201222151326.rootfs.wic.xz"
#IMAGE="core-image-minimal-de0-nano-soc-kit-20201222172751.rootfs.wic.xz"
#IMAGE="core-image-minimal-de0-nano-soc-kit-20201222173659.rootfs.wic.xz"
#IMAGE="core-image-minimal-de0-nano-soc-kit-20201222182527.rootfs.wic.xz"
#IMAGE="core-image-minimal-de0-nano-soc-kit-20201222183539.rootfs.wic.xz"
#IMAGE="core-image-minimal-de0-nano-soc-kit-20201223093225.rootfs.wic.xz"
#IMAGE="core-image-minimal-de0-nano-soc-kit-20201223094646.rootfs.wic.xz"
#IMAGE="core-image-minimal-de0-nano-soc-kit-20201223095454.rootfs.wic.xz"
#IMAGE="core-image-minimal-de0-nano-soc-kit-20201223101810.rootfs.wic.xz"
# good:
#IMAGE="core-image-minimal-de0-nano-soc-kit-20210215173013.rootfs.wic.xz"
# bad:
#IMAGE="core-image-minimal-de0-nano-soc-kit-20210216160232.rootfs.wic.xz"
# good:
#IMAGE="core-image-minimal-de0-nano-soc-kit-20210216161005.rootfs.wic.xz"
#IMAGE="core-image-minimal-de0-nano-soc-kit-20210216161745.rootfs.wic.xz"
#IMAGE="core-image-minimal-de0-nano-soc-kit-20210216162422.rootfs.wic.xz"
#IMAGE="core-image-minimal-de0-nano-soc-kit-20210216163230.rootfs.wic.xz"
#IMAGE="core-image-minimal-de0-nano-soc-kit-20210507212005.rootfs.wic.xz"
#IMAGE="core-image-minimal-de0-nano-soc-kit-20211005204018.rootfs.wic.xz"
#IMAGE="core-image-minimal-de0-nano-soc-kit-20211006055846.rootfs.wic.xz"
#IMAGE="core-image-minimal-de0-nano-soc-kit-20211006062908.rootfs.wic.xz"
#IMAGE="core-image-minimal-de0-nano-soc-kit-20211006084852.rootfs.wic.xz"
#IMAGE="core-image-minimal-de0-nano-soc-kit-20211006085912.rootfs.wic.xz"
#IMAGE="core-image-minimal-de0-nano-soc-kit-20211006093222.rootfs.wic.xz"
#IMAGE="core-image-minimal-de0-nano-soc-kit-20211006094741.rootfs.wic.xz"
#IMAGE="core-image-minimal-de0-nano-soc-kit-20211006100005.rootfs.wic.xz"
#IMAGE="core-image-minimal-de0-nano-soc-kit-20211006101618.rootfs.wic.xz"
#IMAGE="core-image-minimal-de0-nano-soc-kit-20211006104702.rootfs.wic.xz"
#IMAGE="core-image-minimal-de0-nano-soc-kit-20211006110621.rootfs.wic.xz"
#IMAGE="core-image-minimal-de0-nano-soc-kit-20211006111912.rootfs.wic.xz"
IMAGE="core-image-minimal-de0-nano-soc-kit-20211006114448.rootfs.wic.xz"

E2FSCK="/home/student/projects/e2fsprogs-1.45.6/e2fsck/e2fsck"
RESIZE2FS="/home/student/projects/e2fsprogs-1.45.6/resize/resize2fs"

filename=$(basename -- "$IMAGE")
echo "filename: ${filename}"

extension="${filename##*.}"
echo "extension: ${extension}"

filename="${filename%.*}"
echo "filename: ${filename}"

set -x
if [ -f ${IMAGE} ]; then
   unxz ${IMAGE}
fi
set +x

echo "+ sudo ../../../../../wic/wic ls ${filename}"
echo "press <ENTER> to go on"
read r
sudo ../../../../../wic/wic ls ${filename}
set -x
sudo ../../../../../wic/wic ls ${filename}:1
sudo ../../../../../wic/wic ls ${filename}:2
sudo ../../../../../wic/wic ls ${filename}:3
sudo ../../../../../wic/wic ls ${filename}:4
#sudo ../../../../../wic/wic ls ${filename}:5
#sudo ../../../../../wic/wic ls ${filename}:6
set +x

echo "+ sudo fdisk -l ${DEVICE}"
echo "press <ENTER> to go on"
read r
sudo fdisk -l ${DEVICE}

echo "+ sudo ../../../../../bmap-tools/bmaptool copy ${filename} ${DEVICE}"
echo "press <ENTER> to go on"
read r
sudo ../../../../../bmap-tools/bmaptool copy ${filename} ${DEVICE}

echo "+ sudo fdisk -l ${DEVICE}"
echo "press <ENTER> to go on"
read r
sudo fdisk -l ${DEVICE}

echo "+ sudo parted -l"
sudo parted -l

echo "which partition number do you want to change?"
read PARTNUM

if [[ "${PARTNUM}" = "none" ]];
then
   echo "+ OK no resizing"
   exit 1
fi

if ! [[ "${PARTNUM}" =~ ^[0-9]+$ ]];
then
        echo "Sorry integers only"
        echo "which partition number do you want to change?"
        read PARTNUM
fi

echo "adjusting sdcard's partitions"
set +e
command="sudo partprobe $DEVICE"
echo "$command"; $command
sudo udevadm settle
command="sudo ${E2FSCK} -f ${DEVICE}${PARTNUM}"
echo "$command"; $command
command="sudo parted ${DEVICE} resizepart ${PARTNUM} '90%'"
echo "$command"; $command
sudo udevadm settle
command="sudo ${RESIZE2FS} ${DEVICE}${PARTNUM}"
echo "$command"; $command
sudo udevadm settle
command="sudo ${E2FSCK} -f ${DEVICE}${PARTNUM}"
echo "$command"; $command
set -e
echo "+ sync"
sync

echo "+ sudo fdisk -l ${DEVICE}"
echo "press <ENTER> to go on"
read r
sudo fdisk -l ${DEVICE}

echo "sudo parted -l "
sudo parted -l

#if $IS_SD_CARD; then
#    printWarning "adjusting sdcard's partitions"
#    set +e
#    command="sudo partprobe /dev/$DEVICE"
#    echo "$command"; $command
#    sudo udevadm settle
#    command="sudo e2fsck -f /dev/${DEVICE}2"
#    echo "$command"; $command
#    command="sudo parted /dev/${DEVICE} resizepart 2 '100%'"
#    echo "$command"; $command
#    sudo udevadm settle
#    command="sudo resize2fs /dev/${DEVICE}2"
#    echo "$command"; $command
#    set -e
#
#    sync
#fi




#echo "+ which partition number?"
#read partnum

#sudo ./growpart -N /dev/sdd 1


#set -x
#echo "- +" | sfdisk -N ${PARTNUM} ${DEVICE}
#set +x

#sudo growpart /dev/sdd 1
#sudo resize2fs /dev/sdd 1
