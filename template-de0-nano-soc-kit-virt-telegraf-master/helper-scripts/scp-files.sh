#!/bin/bash

DEPLOY="de0-nano-soc-kit-virt-telegraf-wic-master/tmp/deploy/images/de0-nano-soc-kit"

pushd /workdir/build/${DEPLOY}

SOURCE_1="core-image-minimal-virt-docker-de0-nano-soc-kit-*.rootfs.wic*"
SOURCE_2="core-image-minimal-de0-nano-soc-kit-*.rootfs.wic*"
SOURCE_3="core-image-minimal-virt-podman-de0-nano-soc-kit-*.rootfs.wic*"

TARGET_1="student@192.168.42.60:/home/student/projects/${DEPLOY}"

set -x
   scp -r ${SOURCE_1} ${TARGET_1}
   scp -r ${SOURCE_2} ${TARGET_1}
   scp -r ${SOURCE_3} ${TARGET_1}
set +x

popd
