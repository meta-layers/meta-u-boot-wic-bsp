
### Shell environment set up for builds. ###

You can now run 'bitbake <target>'

Common targets are:
    core-image-minimal
    core-image-minimal-base
    core-image-minimal-base-tig-prebuilt

You can also run generated qemu images with a command like 'runqemu qemux86'
