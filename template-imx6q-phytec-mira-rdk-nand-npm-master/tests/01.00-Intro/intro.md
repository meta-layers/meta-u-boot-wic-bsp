# Objectives

A `poky`/`resy` related file system and extensible SKDs (eSDKs) plus examples of a developer workflow.

# Stakeholders

Let's see what people in different roles would like.

|Role                 | BitBake  | cross toolchain | build/test IP block | build/test code | modify image | build image            |
|---------------------|----------|-----------------|---------------------|-----------------|--------------|------------------------|
|Hardware Developer   | no       | maybe           | maybe               | maybe           | maybe        | maybe                  |
|FPGA Developer       | no       | yes? eSDK       | yes ->              | yes eSDK        | yes eSDK ->  | yes eSDK               |
|Software Developer   | no       | yes  eSDK       | yes? ->             | yes eSDK        | yes eSDK ->  | yes eSDK               |
|Release Engineer     | yes      | no?             | no                  | BitBake         | no           | BitBake                |
|CI System            | yes      | no              | maybe (testimage?)  | BitBake         | no           | BitBake                |
|Yocto Person         | yes      | no              | no                  | BitBake         | yes (recipe) | BitBake (image recipe) |

<!-- # Currently available Software Support for `m100pfsevp`

|Where                                                          | What                           | Provider                  | Status           |
|---------------------------------------------------------------|--------------------------------|---------------------------|------------------| 
|Buildroot???                                                   | Buildroot                      | Aries                     | Release          |
|https://github.com/ARIES-Embedded/meta-polarfire-soc-yocto-bsp | OpenEmbedded                   | Aries                     | Release          |
|                                                               | 3.2/gatesgarth                 |                           |                  |
| xxx                                                           | Yocto/poky                     | Reliable Embedded Systems | Experimental     |
|                                                               | 3.3/hardknott                  |                           |                  |
|                                                               | eSDK                           |                           |                  |
| xxx                                                           | Yocto/resy-systemd             | Reliable Embedded Systems | Experimental     |
|                                                               | 3.3/hardknott                  |                           |                  |
|                                                               | eSDK                           |                           |                  |
| xxx                                                           | Yocto/poky                     | Reliable Embedded Systems | Experimental     |
|                                                               | master(06/2021)/BSP: hardknott |                           |                  |
|                                                               | eSDK                           |                           |                  |
| xxx                                                           | Yocto/resy-systemd             | Reliable Embedded Systems | Experimental     |
|                                                               | master(06/2021)/BSP: hardknott |                           |                  |
|                                                               | eSDK                           |                           |                  | -->


# BitBake

## Setup full Reliable Embedded Systems build framework

If you want to `BitBake` you will need to setup the full build framework. There is a separate document describing this.

## `Poky` build container

If you want to `BitBake` you will need to download or build the `poky build container` and start it up. There is a separate document describing this.

# eSDK

## Setup minimal Reliable Embedded Systems build framework

If you just want to use the `eSDK` you just need to setup a minimal build framework. There is a separate document describing this.

Please note, that I typically build `buildtools-extended-tarball` and something like `bitbake core-image-minimal -c populate_sdk_ext` or `bitbake core-image-minimal-base -c populate_sdk_ext`.

## `SDK` container

If you want to use the `eSDK` you will need to download or build the `SDK container` and start it up. There is a separate document describing this.

<!-- # Extra tools in eSDK

- native: hss payload generator
  - https://github.com/RobertBerger/meta-polarfire-soc-yocto-bsp-aries/blob/2021-06-25-hardknott-aries-res/recipes-bsp/hss/hss-payload-generator.inc -->

# Extra tools on target image

TBD

# Examples

## eSDK: Install/Basic Test

[eSDK: install/test](../05.00-eSDK/eSDK-install.md) [OK]

- install `buildtools-extended-tarball`
- install `eSDK`
- build SD card image
- boot SD card image

## eSDK: Cmake Hello World

[eSDK: hellocmake example](../10.00-eSDK-hello-cmake/eSDK-hello-cmake.md) [OK]

<!-- ## Autotooled User Space

[bbexample](https://github.com/whbruce/bbexample) -->

## eSDK: Build/Deploy kernel from external src

[eSDK: Build/Deploy kernel](../14.00-eSDK-build-kernel/eSDK-kernel-build.md) [OK]

Note: 

- We already built the kernel before.

## eSDK: Modify Kernel Config, Build/Deploy kernel from external src

[eSDK: Modify Kernel Config, Build/Deploy kernel](../15.00-eSDK-kernel-config/eSDK-kernel-config.md) [OK]

Note: 

- Kernel together with device trees is deployed to board

<!-- ## fitImage container vs. components inside it

At the moment and in the "yocto workflow" we deploy the kernel and device trees in fitImage.

This is not very well suitable for the "developer workflow".

### What to build?

- `Image` (kernel?)
- device tree

This can be built "classic" and via `devtool`

## How to load it?

- In `u-boot` it should be possible to load `Image` and a custom device tree.

This needs to be tested. -->

## In tree Kernel Module

Not good for 1 hr workshop, since it will need to recompile the kernel?
This is basically a kernel patch

<!-- ### Do we really need to build all the kernel?

- `devtool`
- classic -->

## Out of Tree Kernel Module

No recompilation of the kernel required - so probably fine.

- `devtool`
- classic

[eSDK: Out of tree kernel module, Build/Deploy out of tree kernel module](../25.00-eSDK-out-of-tree-ko/out-of-tree-ko.md) [OK]

## Device Tree

Only build new device tree.

[eSDK: Device tree, Build/Deploy Device Tree](../35.00-eSDK-device-tree/device-tree.md) [OK]

## Device Tree Overlay

Need to see how this could work.

## Cmake User Space Test Case

Should be working if Cmake Hello World works.

# Plan B

Prebuild stuff?