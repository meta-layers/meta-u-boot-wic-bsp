# Objectives

- Build 
  - u-boot
  - kernel
  - image 
- Flash image to SD card
- see it booting

# Minimal build framework

@@@ see:

# Install the eSDK

@@@ see:

# Build kernel

## Setup buildtools extended environment (each time)

setup the environment for buildtools-extended:
```
[@sdk-container:/ ]$
source /opt/resy/imx6q-phytec-mira-rdk-nand/buildtools-extended/environment-setup-x86_64-resysdk-linux
```

you should see something like this:
```
[@sdk-container:/ ]$
Icecc not found. Disabling distributed compiling
```

## Setup SDK environment in SDK container (each time)

Setup the SDK environment in the container:

```
[@sdk-container:/ ]$
source /opt/resy/imx6q-phytec-mira-rdk-nand/esdk/environment-setup-armv7at2hf-neon-resy-linux-gnueabi
```

You should see:

```
[@sdk-container:/ ]$
Icecc not found. Disabling distributed compiling
SDK environment now set up; additionally you may now run devtool to perform development tasks.
Run devtool --help for further details.
```

## Get the kernel sources

```
[@sdk-container:/ ]$
time devtool modify virtual/kernel
```

Please note:
This takes a long (fetch) time under "normal" circumstances since it will download the sources, but we did it before.

You should see something like this:

```
[@sdk-container:/ ]$
NOTE: Starting bitbake server...
NOTE: Reconnecting to bitbake server...
NOTE: Retrying server connection (#1)...
Loading cache: 100% |##########################################################################################################################################################| Time: 0:00:00
Loaded 3750 entries from dependency cache.
Parsing recipes: 100% |########################################################################################################################################################| Time: 0:00:05
Parsing of 2433 .bb files complete (2430 cached, 3 parsed). 3752 targets, 143 skipped, 0 masked, 0 errors.
INFO: Mapping virtual/kernel to linux-yocto-custom
ERROR: recipe linux-yocto-custom is already in your workspace

real    0m12.432s
user    0m1.112s
sys     0m0.220s
```

## Check status

```
[@sdk-container:/ ]$
devtool status
```

You should see something like this:

```
[@sdk-container:/ ]$
NOTE: Starting bitbake server...
hellocmake: /opt/resy/imx6q-phytec-mira-rdk-nand/esdk/workspace/sources/hellocmake (/opt/resy/imx6q-phytec-mira-rdk-nand/esdk/workspace/recipes/hellocmake/hellocmake_git.bb)
linux-yocto-custom: /opt/resy/imx6q-phytec-mira-rdk-nand/esdk/workspace/sources/linux-yocto-custom
```

## A little hack concerning kernel meta-data

We make a symlink to some kernel patches, which `devtool` did not figure out.

```
[@sdk-container:/ ]$
pushd /opt/resy/imx6q-phytec-mira-rdk-nand/esdk/workspace/sources/linux-yocto-custom/oe-local-files
# this might take some time in my container the first time, since git will try to figure out the branch
rsync -avp ../../../../layers/meta-multi-v7-ml-bsp-master/recipes-kernel/linux/patch/5.10.x/patches .
popd
```

# map virtual/kernel -> linux-yocto-custom

```
[@sdk-container:/ ]$
time devtool modify virtual/kernel
```

you should see something like:

```
[@sdk-container:/ ]$
NOTE: Starting bitbake server...
NOTE: Reconnecting to bitbake server...
NOTE: Retrying server connection (#1)...
Loading cache: 100% |##########################################################################################################################################################| Time: 0:00:00
Loaded 3750 entries from dependency cache.
Parsing recipes: 100% |########################################################################################################################################################| Time: 0:00:01
Parsing of 2432 .bb files complete (2430 cached, 2 parsed). 3751 targets, 143 skipped, 0 masked, 0 errors.
Removing 1 recipes from the imx6q_phytec_mira_rdk_nand sysroot: 100% |#########################################################################################################| Time: 0:00:00
INFO: Mapping virtual/kernel to linux-yocto-custom
ERROR: recipe linux-yocto-custom is already in your workspace

real    0m8.643s
user    0m1.168s
sys     0m0.184s
```


## Build the kernel

```
[@sdk-container:/ ]$
time devtool build linux-yocto-custom
```

You should see:

```
[@sdk-container:/ ]$
NOTE: Starting bitbake server...
NOTE: Reconnecting to bitbake server...
NOTE: Retrying server connection (#1)...
Loading cache: 100% |##########################################################################################################################################################| Time: 0:00:00
Loaded 3750 entries from dependency cache.
Parsing recipes: 100% |########################################################################################################################################################| Time: 0:00:05
Parsing of 2433 .bb files complete (2430 cached, 3 parsed). 3752 targets, 143 skipped, 0 masked, 0 errors.
Loading cache: 100% |##########################################################################################################################################################| Time: 0:00:18
Loaded 3750 entries from dependency cache.
Parsing recipes: 100% |########################################################################################################################################################| Time: 0:00:00
Parsing of 2433 .bb files complete (2430 cached, 3 parsed). 3752 targets, 143 skipped, 0 masked, 0 errors.
NOTE: Resolving any missing task queue dependencies
Initialising tasks: 100% |#####################################################################################################################################################| Time: 0:00:04
Sstate summary: Wanted 1 Local 0 Network 0 Missed 1 Current 134 (0% match, 99% complete)  
WARNING: The quilt-native:do_configure sig is computed to be 7824d10c445c7c22febfbd0308a8807df9ac8911c7a9d6a012f3d0bfff754e24, but the sig is locked to 56c96f467f80f8a9c0ffab3e4def8e4409486f8664d2dd85d223cc1671d9c51a in SIGGEN_LOCKEDSIGS_t-x86-64
The quilt-native:do_compile sig is computed to be 4a845508879e1d307f87fe8425af0652c0313d3ddfd0c63681b718523d002358, but the sig is locked to 7f79166e6c0cc5863fe4835bc7c9a4ec32bd410876360c2f97f0ef9d37a2b5a5 in SIGGEN_LOCKEDSIGS_t-x86-64
The quilt-native:do_install sig is computed to be e8f8623975848149f9b559e99a6c2148b02440deea8711af41c4ac287af2ef52, but the sig is locked to 6cdc05c965d2a3c6668f47c3bff999f7eb1117914accb5f5160308b442c5d091 in SIGGEN_LOCKEDSIGS_t-x86-64
...
The rpm-native:do_install sig is computed to be 4ce64ab77ff8f2b2b706fa7c2cab1b3571d5c6a4b992440d1bf7ba6aeff36cec, but the sig is locked to 2a2e605a1b743b09bdadacccba8a1b2f36aa27799150dd47800ef87b70acb9f1 in SIGGEN_LOCKEDSIGS_t-x86-64
The dwarfsrcfiles-native:do_compile sig is computed to be 2b77b2417cb928a713baf79c9bbbf6d3622aa108c4b9301f0d9a083d50990edc, but the sig is locked to d23e5d4005be9d8c446d57da7c6cb705cc1aa54e5aab10c1b2e5d29cf6b10a98 in SIGGEN_LOCKEDSIGS_t-x86-64
The dwarfsrcfiles-native:do_install sig is computed to be 9e1018cfa9c7799baf67f66919f06aab8f831f6c3a5e5b747b25eaded11a8e18, but the sig is locked to 4bd60c48fdc2252d192ba1f7f51bb7570b78be4ae2e1caadb15bbfedf5fe186d in SIGGEN_LOCKEDSIGS_t-x86-64
NOTE: Executing Tasks
NOTE: Tasks Summary: Attempted 628 tasks of which 628 didn't need to be rerun and all succeeded.

Summary: There was 1 WARNING message shown.

real    0m37.939s
user    0m13.020s
sys     0m4.384s
```


(Optional) - build an image

## Build the Image

```
[@sdk-container:/ ]$
devtool build-image
```

## Flash the Image to the SD Card

See eSDK-install

## Run the Image

See eSDK-install