# Objectives

(- Custom Layer)
- Kernel configuration
- Build 
  - kernel
- copy new kernel/dtb to target
- see it booting

# Prerequisites

- [eSDK kernel build](../14.00-eSDK-build-kernel/eSDK-kernel-build.md) executed before

# Minimal build framework

@@@ see:

# Install the eSDK

@@@ see:

# Build kernel with additional configuration

## Setup buildtools extended environment (each time)

setup the environment for buildtools-extended:
```
[@sdk-container:/ ]$
source /opt/resy/imx6q-phytec-mira-rdk-nand/buildtools-extended/environment-setup-x86_64-resysdk-linux
```

you should see something like this:
```
[@sdk-container:/ ]$
Icecc not found. Disabling distributed compiling
```

## Setup SDK environment in SDK container (each time)

Setup the SDK environment in the container:

```
[@sdk-container:/ ]$
source /opt/resy/imx6q-phytec-mira-rdk-nand/esdk/environment-setup-armv7at2hf-neon-resy-linux-gnueabi
```

You should see:

```
[@sdk-container:/ ]$
Icecc not found. Disabling distributed compiling
SDK environment now set up; additionally you may now run devtool to perform development tasks.
Run devtool --help for further details.
```

## (Optional) Get the kernel sources

see:

- [eSDK kernel build](../14.00-eSDK-build-kernel/eSDK-kernel-build.md) before

<!-- ## Get the u-boot sources

see:

- [eSDK kernel build](../14.00-eSDK-build-kernel/eSDK-kernel-build.md) before -->

## Check status

```
[@sdk-container:/ ]$
devtool status
```

You should see something like this:

```
[@sdk-container:/ ]$
NOTE: Starting bitbake server...
hellocmake: /opt/resy/imx6q-phytec-mira-rdk-nand/esdk/workspace/sources/hellocmake (/opt/resy/imx6q-phytec-mira-rdk-nand/esdk/workspace/recipes/hellocmake/hellocmake_git.bb)
linux-yocto-custom: /opt/resy/imx6q-phytec-mira-rdk-nand/esdk/workspace/sources/linux-yocto-custom
```

## Check original kernel config variables

Here I assume the kernel was already built as delivered together with the SDK and that we will modify the "Preemption Model" afterwards.

```
[@sdk-container:/ ]$
/opt/resy/imx6q-phytec-mira-rdk-nand/esdk/workspace/sources/linux-yocto-custom/scripts/extract-ikconfig /opt/resy/imx6q-phytec-mira-rdk-nand/esdk/tmp/deploy/images/imx6q-phytec-mira-rdk-nand/uImage | grep PREEMPT
```
you should see something like this:

```
[@sdk-container:/ ]$
CONFIG_PREEMPT_NONE=y
# CONFIG_PREEMPT_VOLUNTARY is not set
# CONFIG_PREEMPT is not set
# CONFIG_PREEMPTIRQ_DELAY_TEST is not set
```

Please note, that `uImage` from above is a symlink

```
[@sdk-container:/ ]$
ls -lah /opt/resy/imx6q-phytec-mira-rdk-nand/esdk/tmp/deploy/images/imx6q-phytec-mira-rdk-nand/uImage
lrwxrwxrwx 2 sdk sdk 77 Jul 25 17:51 /opt/resy/imx6q-phytec-mira-rdk-nand/esdk/tmp/deploy/images/imx6q-phytec-mira-rdk-nand/uImage -> uImage--5.10.27-std+git999-r0.0-imx6q-phytec-mira-rdk-nand-20210725172434.bin
```

Please note, this is what you should see on the original SD card image as well.

If not, copy this kernel over to the target. 

On this board `/boot/uImage` is what the kernel being used and it's a symlink: `uImage -> uImage-5.10.27-std`.

```
[@sdk-container:/ ]$
scp /opt/resy/imx6q-phytec-mira-rdk-nand/esdk/tmp/deploy/images/imx6q-phytec-mira-rdk-nand/uImage--5.10.27-std+git999-r0.0-imx6q-phytec-mira-rdk-nand-20210725172434.bin root@<target-ip>:/boot/uImage-no-preempt
```

Oh the target adjust the symlink:

```
[@buildhost:serial-console]
cd /boot
ln -sf uImage-no-preempt uImage
```

Reboot the target and check the kernel config like this:

```
[@buildhost:serial-console]
zcat /proc/config.gz | grep PREEMPT
```

you should see something like this:

```
[@buildhost:serial-console]
CONFIG_PREEMPT_NONE=y
# CONFIG_PREEMPT_VOLUNTARY is not set
# CONFIG_PREEMPT is not set
# CONFIG_PREEMPTIRQ_DELAY_TEST is not set
```

## Modify Kernel config

```
[@sdk-container:/ ]$
devtool menuconfig linux-yocto-custom
```

Menuconfig should open.
Change "Preemption Model"


```
[@sdk-container:/ ]$
General setup -->
  Preemption Model (No Forced Preemption (Server))  --->
     (X) Preemptible Kernel (Low-Latency Desktop)

Exit
Save new configuration
```

You should see:
```
[@sdk-container:/ ]$
The libtirpc-native:do_install sig is computed to be 04a85d4940f92211f5ba18d21e5961b0c91b21c473748bec0fc0e0629f7f23b1, but the sig is locked to d6c7c2ce6a005007b24e36f1c75ccd7accb0e0327f5777cb1d2ceb660b75dbcc in SIGGEN_LOCKEDSIGS_t-x86-64
The unifdef-native:do_compile sig is computed to be 9ce2654f78bb4814fc9862336019364f2e280202fc7e4035aba4e965213deb49, but the sig is locked to 93610e72b0cda98ad21dafbf34a3547168ee4c80471e61d5bc7a32d87baf29e3 in SIGGEN_LOCKEDSIGS_t-x86-64
The gtk-doc-native:do_compile sig is computed to be 69fd1decc5c8f7b6b9b2fe2fd89a31b3da83a6699ee43eca53eb[exited]
NOTE: Tasks Summary: Attempted 427 tasks of which 426 didn't need to be rerun and all succeeded.

Summary: There was 1 WARNING message shown.
INFO: Updating config fragment /opt/resy/imx6q-phytec-mira-rdk-nand/esdk/workspace/sources/linux-yocto-custom/oe-local-files/devtool-fragment.cfg
```

Please note:

A custom kernel config was created based on our kernel configuration change.

Inspect the kernel configuration fragment

```
[@sdk-container:/ ]$
cat /opt/resy/imx6q-phytec-mira-rdk-nand/esdk/workspace/sources/linux-yocto-custom/oe-local-files/devtool-fragment.cfg
```

You should see something like this:

```
[@sdk-container:/ ]$
# CONFIG_PREEMPT_NONE is not set
CONFIG_PREEMPT=y
CONFIG_PREEMPT_COUNT=y
CONFIG_PREEMPTION=y
CONFIG_PREEMPT_RCU=y
CONFIG_TASKS_RCU=y
CONFIG_UNINLINE_SPIN_UNLOCK=y
# CONFIG_CEC_GPIO is not set
CONFIG_DEBUG_PREEMPT=y
# CONFIG_PREEMPT_TRACER is not set
```

Please note:

- `/opt/resy/imx6q-phytec-mira-rdk-nand/esdk/workspace/sources/linux-yocto-custom/oe-local-files/devtool-fragment.cfg` 
  - will be overwritten with the next change
  - needs to be renamed and added to a proper kernel recipe or kernel recipe `.bbappend`


but for now:

## Build the kernel with a new config

```
[@sdk-container:/ ]$
time devtool build linux-yocto-custom
```

You should see:

```
[@sdk-container:/ ]$
NOTE: Starting bitbake server...
NOTE: Reconnecting to bitbake server...
NOTE: Retrying server connection (#1)...
Loading cache: 100% |##########################################################################################################################################################| Time: 0:00:01
Loaded 3750 entries from dependency cache.
Parsing recipes: 100% |########################################################################################################################################################| Time: 0:00:05
Parsing of 2433 .bb files complete (2430 cached, 3 parsed). 3752 targets, 143 skipped, 0 masked, 0 errors.
Loading cache: 100% |##########################################################################################################################################################| Time: 0:00:17
Loaded 3750 entries from dependency cache.
Parsing recipes: 100% |########################################################################################################################################################| Time: 0:00:00
Parsing of 2433 .bb files complete (2430 cached, 3 parsed). 3752 targets, 143 skipped, 0 masked, 0 errors.
NOTE: Resolving any missing task queue dependencies
WARNING: /opt/resy/imx6q-phytec-mira-rdk-nand/esdk/layers/meta-multi-v7-ml-bsp-master/recipes-kernel/linux/linux-yocto-custom_5.10.27.bb:do_compile is tainted from a forced run ETA:  0:00:00
Initialising tasks: 100% |#####################################################################################################################################################| Time: 0:00:02
Sstate summary: Wanted 1 Local 0 Network 0 Missed 1 Current 134 (0% match, 99% complete)
WARNING: The quilt-native:do_configure sig is computed to be 7824d10c445c7c22febfbd0308a8807df9ac8911c7a9d6a012f3d0bfff754e24, but the sig is locked to 56c96f467f80f8a9c0ffab3e4def8e4409486f8664d2dd85d223cc1671d9c51a in SIGGEN_LOCKEDSIGS_t-x86-64
The quilt-native:do_compile sig is computed to be 4a845508879e1d307f87fe8425af0652c0313d3ddfd0c63681b718523d002358, but the sig is locked to 7f79166e6c0cc5863fe4835bc7c9a4ec32bd410876360c2f97f0ef9d37a2b5a5 in SIGGEN_LOCKEDSIGS_t-x86-64
The quilt-native:do_install sig is computed to be e8f8623975848149f9b559e99a6c2148b02440deea8711af41c4ac287af2ef52, but the sig is locked to 6cdc05c965d2a3c6668f47c3bff999f7eb1117914accb5f5160308b442c5d091 in SIGGEN_LOCKEDSIGS_t-x86-64
...
The rpm-native:do_install sig is computed to be 4ce64ab77ff8f2b2b706fa7c2cab1b3571d5c6a4b992440d1bf7ba6aeff36cec, but the sig is locked to 2a2e605a1b743b09bdadacccba8a1b2f36aa27799150dd47800ef87b70acb9f1 in SIGGEN_LOCKEDSIGS_t-x86-64
The dwarfsrcfiles-native:do_install sig is computed to be 9e1018cfa9c7799baf67f66919f06aab8f831f6c3a5e5b747b25eaded11a8e18, but the sig is locked to 4bd60c48fdc2252d192ba1f7f51bb7570b78be4ae2e1caadb15bbfedf5fe186d in SIGGEN_LOCKEDSIGS_t-x86-64
NOTE: Executing Tasks
NOTE: linux-yocto-custom: compiling from external source tree /opt/resy/imx6q-phytec-mira-rdk-nand/esdk/workspace/sources/linux-yocto-custom
NOTE: Tasks Summary: Attempted 628 tasks of which 616 didn't need to be rerun and all succeeded.

Summary: There were 2 WARNING messages shown.

real    26m48.626s
user    0m15.240s
sys     0m5.008s
```

Let's see if the kernel config changed:


```
[@sdk-container:/ ]$
/opt/resy/imx6q-phytec-mira-rdk-nand/esdk/workspace/sources/linux-yocto-custom/scripts/extract-ikconfig /opt/resy/imx6q-phytec-mira-rdk-nand/esdk/tmp/deploy/images/imx6q-phytec-mira-rdk-nand/uImage | grep PREEMPT
```
you should see something like this:

```
[@sdk-container:/ ]$
# CONFIG_PREEMPT_NONE is not set
# CONFIG_PREEMPT_VOLUNTARY is not set
CONFIG_PREEMPT=y
CONFIG_PREEMPT_COUNT=y
CONFIG_PREEMPTION=y
CONFIG_PREEMPT_RCU=y
CONFIG_DEBUG_PREEMPT=y
# CONFIG_PREEMPT_TRACER is not set
# CONFIG_PREEMPTIRQ_DELAY_TEST is not set
```

Please note, that `uImage` from above is a symlink

```
[@sdk-container:/ ]$
ls -lah /opt/resy/imx6q-phytec-mira-rdk-nand/esdk/tmp/deploy/images/imx6q-phytec-mira-rdk-nand/uImage
lrwxrwxrwx 2 sdk sdk 77 Jul 25 19:31 /opt/resy/imx6q-phytec-mira-rdk-nand/esdk/tmp/deploy/images/imx6q-phytec-mira-rdk-nand/uImage -> uImage--5.10.27-std+git999-r0.1-imx6q-phytec-mira-rdk-nand-20210725190531.bin
```

Copy this kernel over to the target. 

On this board `/boot/uImage` is what the kernel being used and it's a symlink: `uImage -> uImage-no-preempt`.

```
[@sdk-container:/ ]$
scp /opt/resy/imx6q-phytec-mira-rdk-nand/esdk/tmp/deploy/images/imx6q-phytec-mira-rdk-nand/uImage--5.10.27-std+git999-r0.1-imx6q-phytec-mira-rdk-nand-20210725190531.bin root@<target-ip>:/boot/uImage-preempt
```

Oh the target adjust the symlink:

```
[@buildhost:serial-console]
cd /boot
ln -sf uImage-preempt uImage
```

Reboot the target and check the kernel config like this:

```
[@buildhost:serial-console]
zcat /proc/config.gz | grep PREEMPT
```

you should see something like this:

```
[@buildhost:serial-console]
# CONFIG_PREEMPT_NONE is not set
# CONFIG_PREEMPT_VOLUNTARY is not set
CONFIG_PREEMPT=y
CONFIG_PREEMPT_COUNT=y
CONFIG_PREEMPTION=y
CONFIG_PREEMPT_RCU=y
CONFIG_DEBUG_PREEMPT=y
# CONFIG_PREEMPT_TRACER is not set
# CONFIG_PREEMPTIRQ_DELAY_TEST is not set
```

## (Optional) Build the Image

```
[@sdk-container:/ ]$
time devtool build-image
```

This will take some (compile) time, since now we re-compile the kernel, plus the time it takes to install packages in the root file system.

You should see something like this:

```
[@sdk-container:/ ]$
NOTE: Starting bitbake server...
NOTE: Reconnecting to bitbake server...
NOTE: Retrying server connection (#1)...
Loading cache: 100% |##########################################################################################################################################################| Time: 0:00:00
Loaded 3750 entries from dependency cache.
Parsing recipes: 100% |########################################################################################################################################################| Time: 0:00:05
Parsing of 2432 .bb files complete (2430 cached, 2 parsed). 3751 targets, 143 skipped, 0 masked, 0 errors.
WARNING: Skipping recipe linux-yocto-custom as it doesn't produce a package with the same name
WARNING: No recipes in workspace, building image core-image-minimal unmodified
Loading cache: 100% |##########################################################################################################################################################| Time: 0:00:18
Loaded 3750 entries from dependency cache.
Parsing recipes: 100% |########################################################################################################################################################| Time: 0:00:00
Parsing of 2432 .bb files complete (2430 cached, 2 parsed). 3751 targets, 143 skipped, 0 masked, 0 errors.
NOTE: Resolving any missing task queue dependencies
WARNING: /opt/resy/imx6q-phytec-mira-rdk-nand/esdk/layers/meta-multi-v7-ml-bsp-master/recipes-kernel/linux/linux-yocto-custom_5.10.27.bb:do_compile is tainted from a forced run ETA:  0:00:01
Initialising tasks: 100% |#####################################################################################################################################################| Time: 0:00:52
Checking sstate mirror object availability: 100% |#############################################################################################################################| Time: 0:00:03
Sstate summary: Wanted 136 Local 1 Network 0 Missed 135 Current 1260 (0% match, 90% complete)
WARNING: The libdrm:do_write_config sig is computed to be e2d0b9597b4f9b8a295cdcfbf2f167eadaa4e95f60cfa9505357d66783032428, but the sig is locked to 460da3a7546a6a47868e264a0cb94a2674dc2a9bfacced439b6cfed896c3b02e in SIGGEN_LOCKEDSIGS_t-armv7at2hf-neon
The gdk-pixbuf:do_write_config sig is computed to be 5794f2689d96c1ba779e064bda65633fd00cdb6842e902f33b6d3ff1819c019f, but the sig is locked to b097a2fd8ef5b8c70f0635df5d60cf7509fa92c5529ef33298ed1e70830a4c1e in SIGGEN_LOCKEDSIGS_t-armv7at2hf-neon
The fribidi:do_write_config sig is computed to be 8617da9d302d74a91e88b5f0a45e8ffe1cfbf8c17504d2ee83702917e0661edb, but the sig is locked to c0a1d3e488a45503de0f625727b5315e1df98d7f0d24957181025631435791a1 in SIGGEN_LOCKEDSIGS_t-armv7at2hf-neon
...
The adwaita-icon-theme:do_packagedata sig is computed to be becbc688f70f16cf62c03d912a8b095fe22ec81d3f4b8342185d29dbe8d5e31f, but the sig is locked to 5b2f34916c6d739b05cda2060cc2cd6c8f4a716a93a4e67ec308d3287dad7fe6 in SIGGEN_LOCKEDSIGS_t-allarch
Removing 4 stale sstate objects for arch imx6q_phytec_mira_rdk_nand: 100% |####################################################################################################| Time: 0:00:00
NOTE: Executing Tasks
WARNING: The libdrm:do_write_config sig is computed to be e2d0b9597b4f9b8a295cdcfbf2f167eadaa4e95f60cfa9505357d66783032428, but the sig is locked to 460da3a7546a6a47868e264a0cb94a2674dc2a9bfacced439b6cfed896c3b02e in SIGGEN_LOCKEDSIGS_t-armv7at2hf-neon
...
The adwaita-icon-theme:do_package sig is computed to be e417dd52081efc32440ab421106bc84cbfb559d2d403cc1187bfd352383b1ac2, but the sig is locked to a08096b096c811ca17f0d30b9afaa97541abf8721be34716faa0d1513d9e07d2 in SIGGEN_LOCKEDSIGS_t-allarch
The vim:do_packagedata sig is computed to be df631aa168525993a4f1b9304865f73f65b98b3e8edd2202acda07d1265d3fe2, but the sig is locked to 652e543de23169402d268f047c6b5aeaaab78ca57a445875dad5f30381fdb366 in SIGGEN_LOCKEDSIGS_t-armv7at2hf-neon
The adwaita-icon-theme:do_packagedata sig is computed to be becbc688f70f16cf62c03d912a8b095fe22ec81d3f4b8342185d29dbe8d5e31f, but the sig is locked to 5b2f34916c6d739b05cda2060cc2cd6c8f4a716a93a4e67ec308d3287dad7fe6 in SIGGEN_LOCKEDSIGS_t-allarch
NOTE: Tasks Summary: Attempted 3802 tasks of which 3785 didn't need to be rerun and all succeeded.

Summary: There were 3 WARNING messages shown.
INFO: Successfully built core-image-minimal. You can find output files in /opt/resy/imx6q-phytec-mira-rdk-nand/esdk/tmp/deploy/images/imx6q-phytec-mira-rdk-nand

real    6m25.092s
user    0m37.188s
sys     0m15.800s
```

Please note:

- If we build an image now we should see the kernel configuration change
  - because we compile `linux-yocto-custom` from external source as you can see in the kernel build `linux-yocto-custom: compiling from external source tree /opt/resy/imx6q-phytec-mira-rdk-nand/esdk/workspace/sources/linux-yocto-custom`
  - but this is only in the `devtool` `workspace` and not in a proper recipe
- `/opt/resy/imx6q-phytec-mira-rdk-nand/esdk/workspace/sources/linux-yocto-custom/oe-local-files/devtool-fragment.cfg`  
  - will be overwritten with the next change
  - needs to be renamed and added to a proper kernel recipe or kernel recipe `.bbappend`

We could add a layer and add it to the layer for it to work.

We will need something like:

```
[@sdk-container:/ ]$
devtool finish linux-yocto-custom <new-layer-name>
```

## Flash the Image to the SD Card

See eSDK-install

## Run the Image

See eSDK-install