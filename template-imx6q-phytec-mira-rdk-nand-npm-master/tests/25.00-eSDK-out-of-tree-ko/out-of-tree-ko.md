# Objectives

Get/Build/Deploy `hellokernelwoot`[1]

[1] https://gitlab.com/exempli-gratia/hellokernelwoot.git

# Minimal build framework

@@@ see:

# Install the eSDK

@@@ see:

# `hellokernelwoot` with the eSDK

Get into the SDK container:

```
[@buildhost:]
cd /workdir
./resy-sdk-container.sh
```

## Setup buildtools extended environment (each time)

setup the environment for buildtools-extended:
```
[@sdk-container:/ ]$
source /opt/resy/imx6q-phytec-mira-rdk-nand/buildtools-extended/environment-setup-x86_64-resysdk-linux
```

you should see something like this:
```
[@sdk-container:/ ]$
Icecc not found. Disabling distributed compiling
```

## Setup SDK environment in SDK container (each time)

Setup the SDK environment in the container:

```
[@sdk-container:/ ]$
source /opt/resy/imx6q-phytec-mira-rdk-nand/esdk/environment-setup-armv7at2hf-neon-resy-linux-gnueabi
```

You should see:

```
[@sdk-container:/ ]$
Icecc not found. Disabling distributed compiling
SDK environment now set up; additionally you may now run devtool to perform development tasks.
Run devtool --help for further details.
```

## `devtool add`

```
[@sdk-container:/ ]$
devtool add hellokernelwoot https://gitlab.com/exempli-gratia/hellokernelwoot.git
```

You should see:

```
[@sdk-container:/ ]$
NOTE: Starting bitbake server...
NOTE: Starting bitbake server...
INFO: Fetching git://gitlab.com/exempli-gratia/hellokernelwoot.git;protocol=https...
Loading cache: 100% |##########################################################################################################################################################| Time: 0:00:11
Loaded 3750 entries from dependency cache.
Parsing recipes: 100% |########################################################################################################################################################| Time: 0:00:11
Parsing of 2433 .bb files complete (2430 cached, 3 parsed). 3752 targets, 143 skipped, 0 masked, 0 errors.
NOTE: Resolving any missing task queue dependencies
Initialising tasks: 100% |#####################################################################################################################################################| Time: 0:00:00
Sstate summary: Wanted 0 Local 0 Network 0 Missed 0 Current 0 (0% match, 0% complete)
NOTE: No setscene tasks
NOTE: Executing Tasks
NOTE: Tasks Summary: Attempted 2 tasks of which 0 didn't need to be rerun and all succeeded.
INFO: Using default source tree path /opt/resy/imx6q-phytec-mira-rdk-nand/esdk/workspace/sources/hellokernelwoot
NOTE: Reconnecting to bitbake server...
NOTE: Retrying server connection (#1)...
NOTE: Reconnecting to bitbake server...
NOTE: Reconnecting to bitbake server...
NOTE: Retrying server connection (#1)...
NOTE: Retrying server connection (#1)...
NOTE: Starting bitbake server...
INFO: Using source tree as build directory since that would be the default for this recipe
INFO: Recipe /opt/resy/imx6q-phytec-mira-rdk-nand/esdk/workspace/recipes/hellokernelwoot/hellokernelwoot_git.bb has been automatically created; further editing may be required to make it fully functional
```

## `devtool edit-recipe`

```
[@sdk-container:/ ]$
devtool edit-recipe hellokernelwoot
```

You should see `vim` opening this `recipe`:

```
[@sdk-container:/ ]$
# Recipe created by recipetool
# This is the basis of a recipe and may need further editing in order to be fully functional.
# (Feel free to remove these comments when editing.)

# WARNING: the following LICENSE and LIC_FILES_CHKSUM values are best guesses - it is
# your responsibility to verify that the values are complete and correct.
LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://LICENSE;md5=801f80980d171dd6425610833a22dbe6"

SRC_URI = "git://gitlab.com/exempli-gratia/hellokernelwoot.git;protocol=https"

# Modify these as desired
PV = "1.0+git${SRCPV}"
SRCREV = "03fa40edce46f642c3ec10b1bb1cad181d240d4e"

S = "${WORKDIR}/git"

inherit module
```

exit `vim` with `:q!`

## `devtool build`

```
[@sdk-container:/ ]$
devtool build hellokernelwoot
```

You should see:

```
[@sdk-container:/ ]$
NOTE: Starting bitbake server...
NOTE: Reconnecting to bitbake server...
NOTE: Retrying server connection (#1)...
Loading cache: 100% |##########################################################################################################################################################| Time: 0:00:00
Loaded 3750 entries from dependency cache.
Parsing recipes: 100% |########################################################################################################################################################| Time: 0:00:05
Parsing of 2433 .bb files complete (2430 cached, 3 parsed). 3752 targets, 143 skipped, 0 masked, 0 errors.
Loading cache: 100% |##########################################################################################################################################################| Time: 0:00:17
Loaded 3750 entries from dependency cache.
Parsing recipes: 100% |########################################################################################################################################################| Time: 0:00:00
Parsing of 2433 .bb files complete (2430 cached, 3 parsed). 3752 targets, 143 skipped, 0 masked, 0 errors.
NOTE: Resolving any missing task queue dependencies
WARNING: /opt/resy/imx6q-phytec-mira-rdk-nand/esdk/layers/meta-multi-v7-ml-bsp-master/recipes-kernel/linux/linux-yocto-custom_5.10.27.bb:do_compile is tainted from a forced run ETA:  0:00:00
Initialising tasks: 100% |#####################################################################################################################################################| Time: 0:00:03
Sstate summary: Wanted 9 Local 1 Network 0 Missed 8 Current 142 (11% match, 94% complete) 
WARNING: The libgcc:do_deploy_source_date_epoch sig is computed to be f5f35265f5a65a795cbe4105638ded374396b6f1990ef4b2fe7f87f1dba24022, but the sig is locked to 813c96e4a6bdc5b9cdfec9b63153ab1f3838a34cdbfab0ba63f091149c1daca8 in SIGGEN_LOCKEDSIGS_t-armv7at2hf-neon
The gcc-runtime:do_deploy_source_date_epoch sig is computed to be e698b8bedbdba5c9f2d6137283b05a92e9340482a5676e95212d95f5c46bbb52, but the sig is locked to ff1135b00a631c5e67b0e33805aa28819c677f00616c252b8af265c40bd31241 in SIGGEN_LOCKEDSIGS_t-armv7at2hf-neon
The libgcc:do_fetch sig is computed to be 6b78d2eb3867ea973e9c7eee92034a89526765b5be364438f13c7a4773a227a0, but the sig is locked to ac749dad0603c033af49a4034733385550f5f46ceba3475b31221d1f93a67487 in SIGGEN_LOCKEDSIGS_t-armv7at2hf-neon
...
The gcc-runtime:do_package sig is computed to be 8cf637ace6e8078be47c70af8c1c20c3f26a93309c36e10d2337e89f9d80b93d, but the sig is locked to 8ca86b056d7b452cbd454b4261e5eed8f9f4ff61373b2b5d534791a7b71351b1 in SIGGEN_LOCKEDSIGS_t-armv7at2hf-neon
The gcc-runtime:do_packagedata sig is computed to be 44b089c56452aebc212e1f9372426454afc82a78bcc7118cb087f91c314b1d14, but the sig is locked to e3fcef5f9d2620380b82f80a3ae878b71c974ad8f228c011b1bc969e70dd4302 in SIGGEN_LOCKEDSIGS_t-armv7at2hf-neon
NOTE: Executing Tasks
NOTE: hellokernelwoot: compiling from external source tree /opt/resy/imx6q-phytec-mira-rdk-nand/esdk/workspace/sources/hellokernelwoot
NOTE: Tasks Summary: Attempted 668 tasks of which 653 didn't need to be rerun and all succeeded.

Summary: There were 2 WARNING messages shown.
```

## `devtool deploy-target`

Make sure you can connect via `ssh` to your target.
```
[@buildhost:]
ssh root@<target-ip>
```

You might need to fix some things like
```
[@buildhost:]
ssh-keygen -f "/home/<user>/.ssh/known_hosts" -R <target-ip>
```

try again:
```
[@buildhost:]
ssh root@<target-ip>
```

Now you should be able to log in to the target.


```
[@sdk-container:/ ]$
devtool deploy-target hellokernelwoot root@<target-ip>
```

You should see:

```
[@sdk-container:/ ]$
NOTE: Starting bitbake server...
NOTE: Reconnecting to bitbake server...
NOTE: Retrying server connection (#1)...
Loading cache: 100% |##########################################################################################################################################################| Time: 0:00:00
Loaded 3750 entries from dependency cache.
Parsing recipes: 100% |########################################################################################################################################################| Time: 0:00:06
Parsing of 2433 .bb files complete (2430 cached, 3 parsed). 3752 targets, 143 skipped, 0 masked, 0 errors.
INFO: Successfully deployed /opt/resy/imx6q-phytec-mira-rdk-nand/esdk/tmp/work/imx6q_phytec_mira_rdk_nand-resy-linux-gnueabi/hellokernelwoot/1.0+git999-r0/image
```

## run it on the target
It was installed here:
```
[@buildhost:serial-console]
ls /lib/modules/$(uname -r)/extra
```

you should see:
```
[@buildhost:serial-console]
hellokernelwoot.ko
```

Please note, that the kernel is not tainted before loading the out of tree kernel module
```
[@buildhost:serial-console]
cat /proc/sys/kernel/tainted
```

You should see
```
[@buildhost:serial-console]
0
```


`modprobe` can not find it, since `depmod` was not executed, so let's do that
```
[@buildhost:serial-console]
depmod -a
```
this will take some time (not too much)


Now `modprobe` should work

```
[@buildhost:serial-console]
modprobe hellokernelwoot
```

You should see

```
[@buildhost:serial-console]
[46870.271940] hellokernelwoot: loading out-of-tree module taints kernel.
[46870.278811] --> Hello World kernelwoot! <--
```

```
[@buildhost:serial-console]
lsmod
```

You should see:
```
[@buildhost:serial-console]
    Tainted: G  
hellokernelwoot 16384 0 - Live 0xbf054000 (O)
...
```

Please note, that the kernel is tainted now
```
[@buildhost:serial-console]
cat /proc/sys/kernel/tainted
```

You should see
```
[@buildhost:serial-console]
4096
```

We can also unload the kernel module:

```
[@buildhost:serial-console]
modprobe -r hellokernelwoot
```

You should see

```
[@buildhost:serial-console]
[47256.824008] --> Goodbye Cruel World kernelwoot! <--
```

```
[@buildhost:serial-console]
lsmod | grep kernelwoot
```

You should see:
```
[@buildhost:serial-console]
...
```
... meaning the kernel module was unloaded.

(Optional) - build an image

## Build the Image

```
[@sdk-container:/ ]$
devtool build-image
```

## Flash the Image to the SD Card

See eSDK-install

## Run the Image

See eSDK-install