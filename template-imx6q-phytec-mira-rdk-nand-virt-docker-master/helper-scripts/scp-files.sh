#!/bin/bash

COMMON_PART="imx6q-phytec-mira-rdk-nand-virt-docker-master/tmp/deploy/images/imx6q-phytec-mira-rdk-nand"

pushd /workdir/build/${COMMON_PART}

SOURCE_1="core-image-minimal-virt-docker-imx6q-phytec-mira-rdk-nand-*.rootfs.wic*"
#SOURCE_2="core-image-minimal-base-imx6q-phytec-mira-rdk-nand-*.rootfs.wic*"

TARGET_1="student@192.168.42.60:/home/student/projects/${COMMON_PART}"

set -x
scp -r ${SOURCE_1} ${TARGET_1}
#scp -r ${SOURCE_2} ${TARGET_1}
set +x

popd
