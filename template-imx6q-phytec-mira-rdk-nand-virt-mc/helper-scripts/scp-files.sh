#!/bin/bash

pushd /workdir/build/imx6q-phytec-mira-rdk-nand-virt-wic-mc/tmp-imx6q-phytec-mira-rdk-nand-resy-virt/deploy/images/imx6q-phytec-mira-rdk-nand

SOURCE_1="core-image-minimal-virt-docker-ce-imx6q-phytec-mira-rdk-nand-*.rootfs.wic*"
SOURCE_2="core-image-minimal-virt-docker-ce-mc-imx6q-phytec-mira-rdk-nand-*.rootfs.wic*"
SOURCE_3="core-image-minimal-virt-docker-ce-telegraf-prebuilt-mc-imx6q-phytec-mira-rdk-nand-*.rootfs.wic*"

TARGET_1="student@192.168.42.60:/home/student/projects/imx6q-phytec-mira-rdk-nand-virt-wic-mc/tmp-imx6q-phytec-mira-rdk-nand-resy-virt/deploy/images/imx6q-phytec-mira-rdk-nand"

set -x
scp -r ${SOURCE_1} ${TARGET_1}
scp -r ${SOURCE_2} ${TARGET_1}
scp -r ${SOURCE_3} ${TARGET_1}
set +x

popd
