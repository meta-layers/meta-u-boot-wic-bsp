#!/bin/bash

DEPLOY="imx6ul-phytec-segin-wic-systemd-master/tmp/deploy/images/imx6ul-phytec-segin"

pushd /workdir/build/${DEPLOY}

SOURCE_1="core-image-minimal-imx6ul-phytec-segin-*.rootfs.wic*"
SOURCE_2="core-image-minimal-base-imx6ul-phytec-segin-*.rootfs.wic*"

TARGET_1="student@192.168.42.60:/home/student/projects/${DEPLOY}"

set -x
scp -r ${SOURCE_1} ${TARGET_1}
scp -r ${SOURCE_2} ${TARGET_1}
set +x

popd
