#!/bin/bash

DEPLOY="imx6ul-phytec-segin-virt-telegraf-wic-master/tmp/deploy/images/imx6ul-phytec-segin"

pushd /workdir/build/${DEPLOY}

SOURCE_1="core-image-minimal-virt-docker-imx6ul-phytec-segin-*.rootfs.wic*"
SOURCE_2="core-image-minimal-imx6ul-phytec-segin-*.rootfs.wic*"
SOURCE_3="core-image-minimal-virt-podman-imx6ul-phytec-segin-*.rootfs.wic*"

TARGET_1="student@192.168.42.60:/home/student/projects/${DEPLOY}"

set -x
   scp -r ${SOURCE_1} ${TARGET_1}
   scp -r ${SOURCE_2} ${TARGET_1}
   scp -r ${SOURCE_3} ${TARGET_1}
set +x

popd
