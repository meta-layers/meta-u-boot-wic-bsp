#!/bin/bash

DEPLOY="omap3-beagle-xm-wic-master/tmp/deploy/images/omap3-beagle-xm"

pushd /workdir/build/${DEPLOY}

SOURCE_1="core-image-minimal-omap3-beagle-xm-*.rootfs.wic*"

TARGET_1="student@192.168.42.60:/home/student/projects/${DEPLOY}"

set -x
scp -r ${SOURCE_1} ${TARGET_1}
set +x

popd
