#./00.00-umount-all.sh

#source ../../env.sh

export SD_RW="sdc"

echo "we are going to destroy /dev/${SD_RW} - be sure that this is what you want"
echo "press <ENTER> to go on"
read r

echo "+ ./mkcard.sh /dev/${SD_RW}"
./mkcard.sh /dev/${SD_RW}

echo "+ sudo fdisk -l /dev/${SD_RW}"
sudo fdisk -l /dev/${SD_RW}


