#./00.00-umount-all.sh

#source ../../env.sh

#if [ -z "$1" ]; then
#    echo "No argument supplied"
#    echo "we need UBOOT_VER"
#else
#    echo "version: $1"
#    export UBOOT_VER=${1}
#    export UBOOT_MAINLINE_VER="v${UBOOT_VER}"
#fi

#echo "UBOOT_MAINLINE_VER: ${UBOOT_MAINLINE_VER}"

#EXTRAVER="mainline-${USER}-${UBOOT_MAINLINE_VER}"
#BUILD_DIR=${HOME}/${TARGET_BOARD}/u-boot-build-${EXTRAVER}

export SD_RW="sdc"
export BUILD_DIR="$(pwd)/builddir"

ls --color ${BUILD_DIR}

echo "we are going to destroy /dev/${SD_RW} - be sure that this is what you want"
echo "we assume the builddir is here: ${BUILD_DIR}"
echo "we assume uEnv.txt.xxx is here: $(ls -lah uEnv.txt*)"
tree ${BUILD_DIR}
echo "press <ENTER> to go on"
read r

echo "+ ./mkcard-zynq-zed.sh /dev/${SD_RW} ${BUILD_DIR}"
./mkcard-zynq-zed.sh /dev/${SD_RW} ${BUILD_DIR}

echo "+ sudo fdisk -l /dev/${SD_RW}"
sudo fdisk -l /dev/${SD_RW}
