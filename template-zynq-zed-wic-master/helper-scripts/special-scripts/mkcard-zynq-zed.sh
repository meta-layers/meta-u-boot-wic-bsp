#! /bin/sh
# mkcard.sh v0.5
# History: 
#         2016-08-30 rewrite since just copying SPL, u-boot.img does not work anymore
#         2013-08-18 beagle-xm from tftp/nfs (just boot partition necessary)
#                    adjusted sfdisk command - hoping no more complaints from kernel
#         2012-01-12 adjusted for Beagle-XM training (3 partitions)
#         2011-03-21 used sudo fdisk and sudo umount -f
# (c) Copyright 2012 Robert Berger <robert.berger@reliableembeddedsystems.com>
# (c) Copyright 2009 Graeme Gregory <dp@xora.org.uk>
# Licensed under terms of GPLv2
#
# Parts of the procudure base on the work of Denys Dmytriyenko
# Now it's mainly based on Rob Nelson's eewiki instructions
# http://wiki.omap.com/index.php/MMC_Boot_Format

HERE=$(pwd)

export LC_ALL=C

SFDISK_VERSION="$(sfdisk --version)"
TESTED_SFDISK_VERSION="2.27.1"

echo "========================"
echo ${SFDISK_VERSION}
if [[ "${SFDISK_VERSION}" =~ "${TESTED_SFDISK_VERSION}" ]]; then
  echo "tested sfdisk version"
else
  echo "You might need to update this script"
  echo "sfdisk version incompatible/not tested"
  exit 1
fi
echo "========================"

MKFS_EXT4_VERSION="$(sudo mkfs.ext4 -V)"
TESTED_EXT4_VERSION="1.42.13"

echo "========================"
echo ${MKFS_EXT4_VERSION}
if [[ "${MKFS_EXT4_VERSION}" =~ "${TESTED_MKFS_EXT4_VERSION}" ]]; then
echo "tested mkfs.ext4 version"
else
  echo "You might need to update this script"
  echo "sfdisk version incompatible/not tested"
  exit 1
fi
echo "========================"

if [ $# -ne 2 ]; then
        echo "Usage: $0 <drive> <u-boot_build_dir>"
        exit 1;
fi

echo "+ Press <ENTER> to go on"
read r
clear

DISK=$1
BUILD_DIR=$2

set -x

# unmount stuff in case it was automounted
if [ -b ${DISK}1 ]; then
   sudo umount -f ${DISK}1
fi

if [ -b ${DISK}p1 ]; then
   sudo umount -f ${DISK}p1
fi

if [ -b ${DISK}2 ]; then
   sudo umount -f ${DISK}2
fi

if [ -b ${DISK}p2 ]; then
   sudo umount -f ${DISK}p2
fi

# Erase partition table/labels on microSD card:
sudo dd if=/dev/zero of=${DISK} bs=1M count=50

if [[ "${SFDISK_VERSION}" =~ "${TESTED_SFDISK_VERSION2}" ]]; then
{
echo 1M,48M,0xE,*
echo ,,,-
} | sudo sfdisk ${DISK}
fi

sleep 1

if [[ "${MKFS_EXT4_VERSION}" =~ "${TESTED_MKFS_EXT4_VERSION}" ]]; then
  sudo mkdir /tmp/mnttmp1
  if [ -b ${DISK}1 ]; then
        sudo umount -f ${DISK}1
        mkfs.vfat -F 16 -n BOOT
        sudo mkfs.vfat -F 16 -n boot ${DISK}1
        sudo mount ${DISK}1 /tmp/mnttmp1
  else
        if [ -b ${DISK}p1 ]; then
                sudo umount -f ${DISK}p1
		sudo mkfs.vfat -F 16 -n boot ${DISK}p1
                sudo mount ${DISK}p1 /tmp/mnttmp1
        else
                echo "Cant find rootfs partition in /dev"
        fi
  fi

  sudo mkdir /tmp/mnttmp2
  if [ -b ${DISK}2 ]; then
        sudo umount -f ${DISK}2
        sudo mkfs.ext4 -L "rootfs" ${DISK}2
        sudo mount ${DISK}2 /tmp/mnttmp2
  else
        if [ -b ${DISK}p2 ]; then
                sudo umount -f ${DISK}p2
                sudo mkfs.ext4 -L "rootfs" ${DISK}p2
                sudo mount ${DISK}p2 /tmp/mnttmp2
        else
                echo "Cant find rootfs partition in /dev"
        fi
  fi
fi

sleep 1

sudo cp ${BUILD_DIR}/spl/boot.bin /tmp/mnttmp1
sudo cp ${BUILD_DIR}/u-boot-dtb.img /tmp/mnttmp1
sudo cp ${BUILD_DIR}/u-boot.img /tmp/mnttmp1
sudo cp ${HERE}/uEnv.txt.zynq-zed /tmp/mnttmp1/uEnv.txt
sudo cp ${BUILD_DIR}/
sudo ls -lah --color /tmp/mnttmp1/
sync
sudo umount /tmp/mnttmp1
sudo rm -rf /tmp/mnttmp1
sudo umount /tmp/mnttmp2
sudo rm -rf /tmp/mnttmp2
sync
sudo fdisk -l ${DISK}
set +x

